#!/bin/sh

set -o errexit
set -o nounset
set -o xtrace

celery -A elyza.taskapp worker -l INFO
