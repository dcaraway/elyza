import { Map } from 'immutable'
import configureStore from './configureStore'

const store = configureStore(new Map())

export default store
