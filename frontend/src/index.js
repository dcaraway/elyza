import React from 'react'
import ReactDOM from 'react-dom'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { Provider } from 'react-redux'
import registerServiceWorker from './utils/registerServiceWorker'
import './index.css'
import store from './store'
import MainLayout from './layouts/MainLayout'
import LoginPage from './pages/LoginPage'
import AuthorizedRoute from './utils/AuthorizedRoute'
import DevTools from './utils/DevTools'

const renderLayout = (state, match) => (
  <MainLayout error={ state.get('error') } match={ match }  />
)

const App = () => {
  const isProduction = process.env.NODE_ENV === 'production';

  return (
    <Provider store={ store }>
      <div>
        <Router basename="/frontend">
          <Switch>
            <Route path='/login' component={ LoginPage }/>

            {/* In production, require authentication, not during dev */}
            {isProduction ?
                <AuthorizedRoute path= '/' render={({match}) => renderLayout(store.getState(), match) }/>
                : <Route path='/' render={({match}) => renderLayout(store.getState(), match) }/>
            }
            <Redirect to='/login' />
          </Switch>
        </Router>
        { isProduction || <DevTools />}
      </div>
    </Provider>
  )
}

ReactDOM.render( <App />, document.getElementById('root'))

registerServiceWorker()
