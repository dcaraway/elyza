import React from 'react'

import { login } from '../utils/auth'

const LoginPage = ({ history }) => (
  <div className="container">
    <div className="row">
      <div className="col-md-offset-5 col-md-3">
        <h4>Welcome back.</h4>
        <input className="input-sm" placeholder="username or email" />
        <br/>
        <input className="input-sm" placeholder="password" />
        <br/>
        <button className="btn btn-primary btn-md" onClick={() => {
          login().then(() => {
            history.push('/projects')
          })
        }}>Login</button>
      </div>
    </div>
  </div>
)

export default LoginPage
