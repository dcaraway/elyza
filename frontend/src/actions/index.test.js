import { projectsAreLoaded, makeProjectIsLoaded } from './';
import { fromJS } from 'immutable';

describe("projectsAreLoaded", () => {

  it("returns false if only 1 project loaded", () => {
    const state = fromJS({
      entities: {
        projects: {
          'project1uuid': {}
        },
      },
    });

    const actual = projectsAreLoaded(state);

    expect(actual).toBeFalsy();
  });

  it("returns true if only 2+ projects loaded", () => {
    const state = fromJS({
      entities: {
        projects: {
          'project1uuid': {},
          'project2uuid': {},
        },
      },
    });

    const actual = projectsAreLoaded(state);

    expect(actual).toBeTruthy();
  });

  it("returns false if no projects loaded", () => {
    const state1 = fromJS({
      entities: {
        projects: {
        },
      },
    });

    const state2 = state1.removeIn(['entities','projects']);

    const actual1 = projectsAreLoaded(state1);
    const actual2 = projectsAreLoaded(state2);

    expect(actual1).toBeFalsy();
    expect(actual2).toBeFalsy();
  });
});

describe("makeProjectIsLoaded", () => {

  it("is a thunk", () => {
    expect(typeof makeProjectIsLoaded('someuuid')).toEqual('function');
  });

  it("'s thunk returns false when project not loaded", () => {
    const state = fromJS({
      entities: {
        projects: {
          'project1uuid': {}
        },
      },
    });

    const actual = makeProjectIsLoaded('project2uuid')(state);
    expect(actual).toBeFalsy();
  });

  it("'s thunk returns false when project found without assignees'", () => {
    const state = fromJS({
      entities: {
        projects: {
          'project1uuid': {}
        },
      },
    });

    const actual = makeProjectIsLoaded('project1uuid')(state);
    expect(actual).toBeFalsy();
  });

  it("'s thunk returns true when project found with assignees'", () => {
    const state = fromJS({
      entities: {
        projects: {
          'project1uuid': {'assignees':[]}
        },
      },
    });

    const actual = makeProjectIsLoaded('project1uuid')(state);
    expect(actual).toBeTruthy();
  });
});
