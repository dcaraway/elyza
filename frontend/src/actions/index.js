import { RSAA } from 'redux-api-middleware';
import {
  projectListNormalizer,
  projectNormalizer,
  personListNormalizer,
  personNormalizer
} from '../utils/schema';

//Authentication
export const GET_LOGGED_USER = 'GET_LOGGED_USER';
export const SET_LOGGED_USER = 'SET_LOGGED_USER';

//Projects Resource
export const PROJECTS_REQUEST = 'PROJECTS_REQUEST';
export const PROJECTS_SUCCESS = 'PROJECTS_SUCCESS';
export const PROJECTS_FAILURE = 'PROJECTS_FAILURE';

//Checks to see if we've already loaded projects and can use the cache
//Size of 2 ensures that if projects were set by accessing individual project
//that we do a full page load
export const projectsAreLoaded = (state) => (
  state.hasIn(['entities', 'projects']) &&
  state.getIn(['entities', 'projects']).size >= 2 //TODO when we start paginatingdon't do this
);

const fetchProjects = () => (
  {
    [RSAA]:{
      endpoint: '/api/projects/',
      method: 'GET',
      types: [PROJECTS_REQUEST,
        {
          type: PROJECTS_SUCCESS,
          payload: projectListNormalizer,
        }
        , PROJECTS_FAILURE],
      credentials: 'same-origin',
      bailout: projectsAreLoaded,
    },
  }
)

export const loadProjects = () => { // encapsulating, will handle pagination and such
  return fetchProjects();
};

//Project Resource
export const PROJECT_REQUEST = 'PROJECT_REQUEST';
export const PROJECT_SUCCESS = 'PROJECT_SUCCESS';
export const PROJECT_FAILURE = 'PROJECT_FAILURE';

//Bails out if a project with given UUID has assignees
//TODO should have metadata about project and whether it's fully loaded or not
export const makeProjectIsLoaded = (uuid) => (
  state => state.hasIn(['entities', 'projects', uuid, 'assignees'])
)

export const loadProject = (uuid) => {
  return {
    [RSAA]:{
      endpoint: `/api/projects/${uuid}/`,
      method: 'GET',
      types: [PROJECT_REQUEST,
        {
          type: PROJECT_SUCCESS,
          payload: projectNormalizer,
        },
        PROJECT_FAILURE],
      credentials: 'same-origin',
      bailout: makeProjectIsLoaded(uuid),
    },
  }
}

//People
export const PEOPLE_REQUEST = 'PEOPLE_REQUEST';
export const PEOPLE_SUCCESS = 'PEOPLE_SUCCESS';
export const PEOPLE_FAILURE = 'PEOPLE_FAILURE';

const fetchPeople = () => (
  {
    [RSAA]:{
      endpoint: '/api/workers/',
      method: 'GET',
      types: [PEOPLE_REQUEST,
        {
          type: PEOPLE_SUCCESS,
          payload: personListNormalizer,
        }
        , PEOPLE_FAILURE],
      credentials: 'same-origin',
    },
  }
)

export const loadPeople = () => { // encapsulating, will handle pagination and such
  return fetchPeople();
};

export const PERSON_REQUEST = 'PERSON_REQUEST';
export const PERSON_SUCCESS = 'PERSON_SUCCESS';
export const PERSON_FAILURE = 'PERSON_FAILURE';

export const makePersonIsLoaded = (uuid) => (
  state => state.hasIn(['entities', 'workers', uuid]) //Need to check more details on cache
)

export const loadPerson = (uuid) => {
  return {
    [RSAA]:{
      endpoint: `/api/workers/${uuid}/`,
      method: 'GET',
      types: [PERSON_REQUEST,
        {
          type: PERSON_SUCCESS,
          payload: personNormalizer,
        },
        PERSON_FAILURE],
      credentials: 'same-origin',
      bailout: makePersonIsLoaded(uuid),
    },
  }
}
