import { projectListNormalizer,
  projectNormalizer,
  projectListSchema,
  projectSchema } from './schema';

import { normalize } from 'normalizr';

describe("projectListNormalizer", () => {

  it("updates existing entities", () => {
    const payload = [{uuid: 'myuuid'}];

    const res1 = {
      headers: {
        get(name) {
          return name === 'Content-Type' ? 'application/json' : undefined;
        }
      },
      json() {
        return Promise.resolve(payload);
      }
    };

    const expected = normalize(payload, projectListSchema);

    expect(projectListNormalizer({}, {}, res1)).resolves.toEqual(expected);
  });

});

describe('projectNormalizer', () => {
  it('extracts errors from api', ()=>{
    const payload = {uuid: 'myuuid'};

    const res1 = {
      headers: {
        get(name) {
          return name === 'Content-Type' ? 'application/json' : undefined;
        }
      },
      json() {
        return Promise.resolve(payload);
      }
    };

    const expected = normalize(payload, projectSchema);

    expect(projectNormalizer({}, {}, res1)).resolves.toEqual(expected);
  });
});
