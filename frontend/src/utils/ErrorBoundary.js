import React from 'react'

class ErrorBoundary extends React.Component {

  componentDidCatch(error, info) {
    console.log('ErrorBoundary triggered (no action)', error, info) //TODO add error to state
  }

  render() {
    return this.props.children;
  }
}

export default ErrorBoundary;
