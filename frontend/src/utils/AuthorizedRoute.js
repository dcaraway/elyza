import React from 'react';
import { Route, Redirect } from 'react-router-dom'
import { connect } from 'react-redux'
import { getLoggedUser } from './auth'

class AuthorizedRoute extends React.Component {

  componentWillMount() {
    getLoggedUser()
  }

  render() {
    const { component: Component, logged, ...rest } = this.props

    return (
      <Route {...rest} render={props => {
        return logged
          ? <Component {...props} />
          : <Redirect to="/login" />
      }} />
    )
  }
}

const stateToProps = (state) => ({
  logged: state.getIn(['user', 'logged']),
})

export default connect(stateToProps)(AuthorizedRoute)
