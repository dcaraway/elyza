import { schema, normalize } from 'normalizr';

//Normalizer generators
const normalizePayload  = (action, state, res, dataSchema) => {
  const contentType = res.headers.get('Content-Type');
  if (contentType && ~contentType.indexOf('json')) {
    // Just making sure res.json() does not raise an error

    return res.json().then((json) => normalize(json, dataSchema));
  }
}

const normalizeToSchema = (yourSchema) => {
  return (action, state, res) => normalizePayload(action, state, res, yourSchema)
}

//Schemas
export const workerSchema = new schema.Entity('workers', {}, { idAttribute: 'uuid'});

export const actualSchema = new schema.Entity('actuals', {
  worker: workerSchema
}, { idAttribute: 'uuid'});

export const appointmentSchema = new schema.Entity('appointments', {
  worker: workerSchema
}, { idAttribute: 'uuid'});

export const projectSchema = new schema.Entity('projects', {
  assignees: [ workerSchema ],
  actuals: [ actualSchema ],
  appointments: [ appointmentSchema ],
}, { idAttribute: 'uuid' });

//Use a partial schema for Project List response, which exclude relations to load
//quickly. We actually grab the project data in full when individually querying a project
export const projectPartialSchema = new schema.Entity('projects',
  {}, { idAttribute: 'uuid'});

export const projectListSchema = new schema.Array(projectPartialSchema);

export const workerListSchema = new schema.Array(workerSchema)

/* ------------------
 * Exports
 * ------------------*/
export const projectListNormalizer = normalizeToSchema(projectListSchema)

export const projectNormalizer = normalizeToSchema(projectSchema)

export const personNormalizer = normalizeToSchema(workerSchema)
export const personListNormalizer = normalizeToSchema(workerListSchema)
