import React from 'react';
import MainLayout from './MainLayout';
import { shallow } from 'enzyme';

describe("<MainLayout />", () => {
  const match = {path: '/'}
  it('contains Header', () => {
    const wrapper = shallow(<MainLayout match={ match } />);
    expect(wrapper.find('div > Header')).toHaveLength(1);
  });

  it('contains main', () => {
    const wrapper = shallow(<MainLayout match={ match } />);
    expect(wrapper.find('div > main')).toHaveLength(1);
  });

  it('contains Footer', () => {
    const wrapper = shallow(<MainLayout match={ match } />);
    expect(wrapper.find('div > Footer')).toHaveLength(1);
  });
});
