import React from 'react';
import { Grid, Col, Row } from 'react-bootstrap';
import { Route } from 'react-router-dom';
import ProjectListContainer from '../containers/ProjectListContainer';
import ProjectDetailContainer from '../containers/ProjectDetailContainer';

const ProjectSubLayout = ({ match }) => (
  <div>
    <Grid>
      <Row>
        <Col xs={12} md={3}>
          <Route path={match.path} component={ ProjectListContainer } />
        </Col>
        <Col xs={12} md={9}>
          <Route path={`${match.path}/:uuid`} component={ ProjectDetailContainer } />
        </Col>
      </Row>
    </Grid>
  </div>
)

export default ProjectSubLayout;
