import React from 'react'
import { Route } from 'react-router'
import {Grid, Row, Col } from 'react-bootstrap'
import PersonListContainer from '../containers/PersonListContainer'
import PersonDetailContainer from '../containers/PersonDetailContainer'

const PersonSubLayout = ({match}) => (
  <div>
    <Grid>
      <Row>
        <Col xs={12} md={3}>
          <Route path={match.path} component={ PersonListContainer } />
        </Col>
        <Col xs={12} md={9}>
          <Route path={`${match.path}/:uuid`} component={ PersonDetailContainer }/>
        </Col>
      </Row>
    </Grid>
  </div>
)

export default PersonSubLayout
