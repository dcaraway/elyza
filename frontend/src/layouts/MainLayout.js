import React from 'react'
import ProjectSubLayout from './ProjectSubLayout'
import Header from '../components/Header'
import Footer from '../components/Footer'
import { Switch, Route } from 'react-router-dom'
import ErrorMessage from '../components/ErrorMessage'
import PersonSubLayout from './PersonSubLayout'
import HomePage from '../pages/HomePage'
import ErrorBoundary from '../utils/ErrorBoundary'

const MainLayout = ({ match, error }) => (
  <div className="container">
    <Header />

    <ErrorMessage error={ error }/>

    <main>
      <ErrorBoundary>
        <Switch>
          <Route exact path={ match.path } component={ HomePage } />
          <Route path={ `${match.path}projects` } component={ ProjectSubLayout } />
          <Route path={ `${match.path}people` } component={ PersonSubLayout } />
        </Switch>
      </ErrorBoundary>
    </main>

    <Footer />
  </div>
)


export default MainLayout
