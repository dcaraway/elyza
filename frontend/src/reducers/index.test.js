import { entities, errorMessage, fetchStatuses } from './';
import { fromJS, Map } from 'immutable';

describe("entities reducer", () => {
  //Setup fixtures
  const project1 = { uuid: 'uuid1', name: 'foo'};
  const project1Updated = { uuid: 'uuid1', name: 'bar'};
  const initialState = fromJS({
    projects: {
      'uuid1':  project1,
    },
  });

  it("updates existing entities", () => {
    const entityProjects = {
      'uuid1': project1Updated,
    }

    const action = {
      type: 'PROJECTS_SUCCESS',
      payload: {
        entities: {
          projects: entityProjects,
        },
      },
    };

    const expectedState = fromJS({
      projects: entityProjects,
    });

    const actualState = entities(initialState, action);

    expect(actualState).toEqual(expectedState);
  });

  it("returns original state if API call not successful", () => {
    const entityProjects = {
      'uuid1': project1Updated,
    }

    const action = {
      type: 'PROJECTS_ERROR',
      payload: {
        entities: {
          projects: entityProjects,
        },
      },
    };

    const expectedState = initialState;

    const actualState = entities(initialState, action);

    expect(actualState).toEqual(expectedState);
  });
});

describe('errorMessage', () => {
  it('extracts errors from api', ()=>{
    const initialState = Map();
    const action = {
      error: 'someerror',
      payload: {
        name: 'error1',
        message: 'error happened',
      }
    }

    const expectedState =  fromJS({
      name: 'error1',
      message: 'error happened',
    })

    const actualState = errorMessage(initialState, action);

    expect(actualState).toEqual(expectedState);

  });

  it('removes previous error if request in progress', () => {
    const initialState = fromJS({
      name: 'error2',
      message: 'error happened'
    })

    const action = {
      type: 'NEW_DATA_REQUEST',
    }

    const expectedState = Map();

    const actualState = errorMessage(initialState, action);

    expect(actualState).toEqual(expectedState);
  });

  it('returns original state if not error or new request', () => {
    const initialState = fromJS({
      name: 'error2',
      message: 'error happened'
    })

    const action = {
      type: 'SOME_ACTION',
    }

    const expectedState = initialState;

    const actualState = errorMessage(initialState, action);

    expect(actualState).toEqual(expectedState);
  });
});

describe('fetchStatuses', () => {
  it('returns true when fetching', ()=>{
    const initialState = false;
    const action = {
      type: 'SOME_REQUEST',
    }

    const expectedState = true;

    const actualState = fetchStatuses(initialState, action);

    expect(actualState).toEqual(expectedState);
  });

  it('returns false when fetch succeeds', ()=>{
    const initialState = true;
    const action = {
      type: 'SOME_SUCCESS',
    }

    const expectedState = false;

    const actualState = fetchStatuses(initialState, action);

    expect(actualState).toEqual(expectedState);
  });

  it('returns false when fetch fails', ()=>{
    const initialState = true;
    const action = {
      type: 'SOME_FAILURE',
    }

    const expectedState = false;

    const actualState = fetchStatuses(initialState, action);

    expect(actualState).toEqual(expectedState);
  });
});
