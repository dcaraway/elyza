import { combineReducers } from 'redux-immutable';
import { Map, fromJS } from 'immutable';
import { GET_LOGGED_USER, SET_LOGGED_USER } from '../actions'

const initialEntityState = fromJS({
  projects:{},
  workers:{},
});

// Updates an entity cache in response to
// any action with payload
export const entities = (state = initialEntityState, action) => {
  if(action.type.endsWith('SUCCESS') && action.payload){
    return state.mergeDeep(fromJS(action.payload.entities));
  }

  return state;
};

// Updates error message to notify about failed fetches
export const errorMessage = (state = Map(), action) => {
  if (action.error){
    const {name, message} = action.payload;
    return state.set('name', name).set('message', message);
  }

  if(action.type.endsWith('REQUEST')){
    return state.remove('name').remove('message');
  }

  return state;
};

// Updates fetching to notify of in-progress and complete fetches
export const fetchStatuses = (state = false, action) => {
  if(action.type.endsWith('REQUEST')){
    return true;
  }

  if(action.type.endsWith('SUCCESS') || action.type.endsWith('FAILURE')) {
    return false;
  }

  return state;
};

// Handles authenticated user state
const initialUserState = fromJS({
  logged: false
})

const loggedUserReducer = (state = initialUserState, action) => {
  if (action.type === GET_LOGGED_USER) {
    return state.set('pending', false)
  }

  if (action.type === SET_LOGGED_USER) {
    return state.set('pending', false).set('logged', action.logged)
  }

  return state
}

const rootReducer = combineReducers({
  entities,
  error: errorMessage,
  user: loggedUserReducer,
  isFetching: fetchStatuses,
});

export default rootReducer;
