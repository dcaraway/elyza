import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

export const ScheduleTableRow = ({ person, appointment }) => (
  <tr key={ appointment.get('uuid') }>
    <td>{ person.get('last_name') }</td>
    <td>{ person.get('first_name') }</td>
    <td>{ appointment.getIn(['during', 'lower']) }</td>
    <td>{ appointment.getIn(['during', 'upper']) }</td>
    <td>{ appointment.get('hours') }</td>
  </tr>
)

ScheduleTableRow.propTypes = {
  person: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,
  }),

  appointment: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    hours: PropTypes.string.isRequired,
    during: PropTypes.object.isRequired,
  })
}

export default ScheduleTableRow;
