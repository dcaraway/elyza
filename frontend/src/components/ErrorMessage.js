import React from 'react'
import { Alert } from 'react-bootstrap'

const ErrorMessage = ({error}) =>{
  return (error && error.size > 0 &&
    <Alert bsStyle="danger">
    { error.get('message') }
    </Alert>
  )
}

export default ErrorMessage
