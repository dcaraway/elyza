import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { ListGroup, ListGroupItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const personListStyle = {
  overflow: 'auto',
  height: '100vh'
}

const renderListItem = (person, basepath, loadPerson) => {
  const uuid = person.get('uuid');
  const name = `${person.get('last_name')}, ${person.get('first_name')}`;
  const path = `${basepath}/${uuid}/`;

  const onClick = () => loadPerson(uuid);

  return (
  <LinkContainer key={ uuid } to={ path }>
    <ListGroupItem onClick={ onClick }>{ name }</ListGroupItem>
  </LinkContainer>
  );
}

const renderListItems = (people, basepath, isFetching, loadPerson) => {
  if(!isFetching && people && people.isEmpty()){
    return (<p>No people found</p>);
  }

  return people.toList().map(person => renderListItem(person, basepath, loadPerson))
}

class PersonList extends React.Component {
  static propTypes = {
    loadPeople: PropTypes.func.isRequired,
    loadPerson: PropTypes.func.isRequired,
    people: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
      }),
      PropTypes.string.isRequired,
    ),
    isFetching: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    this.props.loadPeople();
  }

  render() {
    const { people, match, isFetching, loadPerson } = this.props

    return (
      <div style={personListStyle}>
        <ListGroup>
          { renderListItems(people, match.path, isFetching, loadPerson) }
        </ListGroup>
      </div>
    )
  }
}

export default PersonList
