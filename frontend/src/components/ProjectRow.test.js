import React from 'react';
import { shallow } from 'enzyme';
import ProjectRow from './ProjectRow';

describe("<ProjectRow />", () => {
  it('contains 1 div', () => {
    const wrapper = shallow(<ProjectRow title="foo" path="/bar" onClick={()=>{}}/>);
    expect(wrapper.find('LinkContainer > ListGroupItem')).toHaveLength(1);
  });
});
