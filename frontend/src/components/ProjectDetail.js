import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { PulseLoader } from 'halogenium';
import ProjectView from './ProjectView';

class ProjectDetail extends React.Component {
  static propTypes = {
    uuid: PropTypes.string.isRequired,
    loadProject: PropTypes.func.isRequired,
    project: ImmutablePropTypes.contains({
      uuid: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      isFetching: PropTypes.bool,
    }),
  }

  componentWillMount() {
    this.props.loadProject(this.props.uuid);
  }

  render() {
    const { project, isFetching } = this.props;

    return (
      <div className="project-detail">
        { project && <ProjectView project={ project } isFetching={ isFetching } />}

        <PulseLoader loading={isFetching} color="#26A65B" size="16px" margin="4px" />
      </div>
    );
  }
}

export default ProjectDetail
