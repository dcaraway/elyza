import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

export const TeamTableRow = ({ person }) => (
  <tr>
    <td>{ person.get('last_name') }</td>
    <td>{ person.get('first_name') }</td>
    <td>{ sumHours(person.get('appointments')) }</td>
    <td>{ sumHours(person.get('actuals')) }</td>
  </tr>
)

TeamTableRow.propTypes = {
  person: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    first_name: PropTypes.string.isRequired,
    last_name: PropTypes.string.isRequired,

    appointments: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        hours: PropTypes.string.isRequired,
        during: PropTypes.object.isRequired,
      })
    ),

    actuals: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        hours: PropTypes.string.isRequired,
        during: PropTypes.object.isRequired,
      }),
    ),
  }),
}

const sumHours = (timeRecords) => {
  if (timeRecords){
    return timeRecords.map(timeRecord => parseFloat(timeRecord.get('hours')))
      .reduce((total, num) => total + num);
  }

  return 0;
}

export default TeamTableRow;
