import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Table } from 'react-bootstrap';
import TeamTableRow from './TeamTableRow';

export const NO_TEAM_MSG = "No team members assigned to this project";

export const TeamTable = ({ project }) => (
  (project.has('assignees') && !project.get('assignees').isEmpty())  ? renderTable(project) : <p>{ NO_TEAM_MSG }</p>
)

TeamTable.propTypes = {
  project: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    budget_in_dollars: PropTypes.number.isRequired,
    assignees: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
      })
    ),
    appointments: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        hours: PropTypes.string.isRequired,
        during: PropTypes.object.isRequired,
      })
    ),
    actuals: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        hours: PropTypes.string.isRequired,
        during: PropTypes.object.isRequired,
      }),
    ),
    workerAppointments: ImmutablePropTypes.map,
    workerActuals: ImmutablePropTypes.map,
    isFetching: PropTypes.bool,
  }).isRequired,
}


export const renderTable = project => (
  <Table>
    <thead>
      <tr>
        <th>Last Name</th>
        <th>First Name</th>
        <th>SUM( Scheduled Hours )</th>
        <th>SUM( Logged Hours )</th>
      </tr>
    </thead>
    <tbody>
      { renderAssignees(project) }
    </tbody>
  </Table>
)

const renderAssignees = project => {
  const workerAppointments = project.get('workerAppointments')
  const workerActuals = project.get('workerActuals')

  return project.get('assignees').sortBy(assignee => assignee.last_name)
    .map(assignee =>{
      const uuid = assignee.get('uuid')
      const appointments = workerAppointments.get(uuid);
      const actuals = workerActuals.get(uuid);
      const person = assignee.set('appointments', appointments).set('actuals', actuals);

      return (<TeamTableRow key={ uuid } person={ person } />);
    });
}

export default TeamTable;
