import React from 'react';

const Footer = () => (
  <footer>
    <hr/>
    <div className="container-fluid">
      © {new Date().getFullYear()} Copyright <a href="https://www.elyza.co"> Elyza </a>
    </div>
  </footer>
)

export default Footer;
