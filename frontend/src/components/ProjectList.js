import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import ProjectTable from '../components/ProjectTable';
import { PulseLoader } from 'halogenium';

const projectListStyle = {
  overflow: 'auto',
  height: '100vh'
}

export class ProjectList extends React.Component {
  static propTypes = {
    loadProjects: PropTypes.func.isRequired,
    loadProject: PropTypes.func.isRequired,
    projects: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        title: PropTypes.string.isRequired,
      }),
      PropTypes.string.isRequired,
    ).isRequired,
    isFetching: PropTypes.bool.isRequired,
  };

  componentDidMount() {
    this.props.loadProjects();
  }

  render() {
    const { projects, isFetching, match, loadProject } = this.props;

    return (
      <div className="project-list">
        <div style={projectListStyle}>
          <ProjectTable projects={ projects } basepath={ match.path } loadProject={ loadProject } isFetching = { isFetching }/>
        </div>
        <PulseLoader loading={ isFetching } color="#26A65B" size="16px" margin="4px" />
      </div>
    );
  }
}

export default ProjectList
