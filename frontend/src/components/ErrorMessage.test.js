import React from 'react';
import ErrorMessage from './ErrorMessage';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';

describe("<ErrorMessage />", () => {
  it('shows alert when error', () => {
    const error = {
      name: 'error1',
      message: 'somethingbad',
    };

    const wrapper = shallow(<ErrorMessage error={fromJS(error)} />);

    expect(wrapper.find('Alert').contains(error.message)).toBe(true);
  });

  it('hide alert when no error', () => {
    const wrapper = shallow(<ErrorMessage />);
    expect(wrapper.find('Alert')).toHaveLength(0);
  });
});
