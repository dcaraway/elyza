import React from 'react';
import { Table } from 'react-bootstrap'
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import ScheduleTableRow from './ScheduleTableRow';

export const NO_TEAM_MSG = "No assignees";

export const ScheduleTable = ({ project }) => (
  (project.has('assignees') && !project.get('assignees').isEmpty()) ? renderTable(project) :
  <p>{ NO_TEAM_MSG }</p>
)

ScheduleTable.propTypes = {
  project: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    budget_in_dollars: PropTypes.number.isRequired,
    assignees: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
      })
    ),
    workerAppointments: ImmutablePropTypes.map,
  }).isRequired,
}

const renderTable = project => (
  <Table>
    <thead>
      <tr>
        <th>Last Name</th>
        <th>First Name</th>
        <th>Start Date</th>
        <th>End Date</th>
        <th>Scheduled Hours</th>
      </tr>
    </thead>
    <tbody>
      { renderAppointments(project) }
    </tbody>
  </Table>
)

const renderAppointments = project => (
  project.get('assignees').sortBy(assignee => assignee.last_name).map(
    assignee => renderWorkerAppointments(assignee, project.get('workerAppointments'))
  )
)

const renderWorkerAppointments = (assignee, workerAppointments) => {
  const uuid = assignee.get('uuid');

  return workerAppointments.has(uuid) && workerAppointments.get(uuid).map(appointment =>(
    <ScheduleTableRow key={ uuid }
      person={ assignee }
      appointment={ appointment } />
  )
  )
}

export default ScheduleTable;
