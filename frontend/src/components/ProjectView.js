import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { Tabs, Tab } from 'react-bootstrap'
import TeamTable from './TeamTable';
import ScheduleTable from './ScheduleTable';
import TimeChart from './TimeChart';

const ProjectView = ({ project }) => (
  <div>
    <h2>{ project.get('title') }</h2>

    <TimeChart project={ project }/>

    <Tabs defaultActiveKey={1} id="project-team-details">
      <Tab eventKey={1} title="Team">
        <h3>Team</h3>
        <TeamTable project={ project } />
      </Tab>

      <Tab eventKey={2} title="Schedule">
        <h3>Schedule</h3>
        <ScheduleTable project={ project } />
      </Tab>
    </Tabs>
  </div>
)

ProjectView.propTypes = {
  project: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }),
}

export default ProjectView
