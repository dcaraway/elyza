import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import ProjectRow from './ProjectRow';
import { ListGroup } from  'react-bootstrap';

const renderProjectRow = (project, basepath, loadProject) => {
  const uuid = project.get('uuid');
  const title = project.get('title');
  const uri = `${basepath}/${uuid}`;

  return (<ProjectRow key={ uuid } title={ title } path={ uri } onClick={ () => loadProject(uuid)} />)
}

const renderProjectRows = (projects, basepath, isFetching, loadProject) => {
  if(!isFetching && projects.isEmpty()){
    return (<p>No projects found</p>);
  }

  return projects.toList().map(project => renderProjectRow(project, basepath, loadProject))
}

const ProjectTable = ({ projects, basepath, isFetching, loadProject }) => (
  <div className="project-list">
    <ListGroup>
      {renderProjectRows(projects, basepath, isFetching, loadProject)}
    </ListGroup>
  </div>
 )

ProjectTable.propTypes = {
  loadProject: PropTypes.func.isRequired,
  projects: ImmutablePropTypes.mapOf(
    ImmutablePropTypes.contains({
      uuid: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    }),
    PropTypes.string.isRequired,
  ).isRequired,
  basepath: PropTypes.string.isRequired,
  isFetching: PropTypes.bool.isRequired,
}

export default ProjectTable;
