import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import { TeamTable, NO_TEAM_MSG } from './TeamTable';
import TeamTableRow  from './TeamTableRow'


describe("<TeamTable />", () => {
  const project = fromJS({
    'uuid': 'someuuid',
    'title': 'sometitle',
    'budget_in_dollars': 0,
    'assignees': [
      {'uuid': 'assignee1',
        'first_name': 'Firstname1',
        'last_name': 'Lastname1',
      },

      {'uuid': 'assignee2',
        'first_name': 'Firstname2',
        'last_name': 'Lastname2',
      },
    ],

    'workerAppointments': {
    },

    'workerActuals': {
    }
  });

  it('displays a message when there are no assignees', () => {
    const project = fromJS({
      'uuid': 'someuuid',
      'title': 'sometitle',
      'budget_in_dollars': 0,
    });

    const wrapper = shallow(<TeamTable project={project} />);
    expect(wrapper.find('p').text()).toEqual(NO_TEAM_MSG);
  });

  it('indicates when assignees is empty', () => {
    const project = fromJS({
      'uuid': 'someuuid',
      'title': 'sometitle',
      'budget_in_dollars': 0,
      'assignees': []
    });

    const wrapper = shallow(<TeamTable project={project} />);
    expect(wrapper.find('p').text()).toEqual(NO_TEAM_MSG);
  });

  it('displays a table', () => {
    const wrapper = shallow(<TeamTable project={project} />);

    expect(wrapper.find('Table')).toHaveLength(1);
  });

  it('displays table header with 4 labels', () => {
    const theadWrapper = shallow(<TeamTable project={project} />).find('Table > thead');

    expect(theadWrapper).toHaveLength(1);
    expect(theadWrapper.find('th')).toHaveLength(4);
  });

  it('displays a TeamTableRow for each assignee', () => {
    const wrapper = shallow(<TeamTable project={project} />);

    expect(wrapper.find(TeamTableRow)).toHaveLength(2);
  });

  it('displays TeamTableRow sorted by assignee last name', () => {
    const outOfOrder = fromJS([
      {
        'uuid': 'uuid2',
        'first_name': 'first',
        'last_name': 'last2'
      },
      {
        'uuid': 'uuid3',
        'first_name': 'first',
        'last_name': 'last3'
      },
      {
        'uuid': 'uuid1',
        'first_name': 'first',
        'last_name': 'last1'
      }
    ]);

    const unorderedProject = project.set('assignees', outOfOrder);

    const rowWrapper = shallow(<TeamTable project={ unorderedProject } />).find(TeamTableRow);

    expect(rowWrapper.at(0)).toHaveProperty('person', outOfOrder[2]);
    expect(rowWrapper.at(1)).toHaveProperty('person', outOfOrder[1]);
    expect(rowWrapper.at(2)).toHaveProperty('person', outOfOrder[0]);
  })

});
