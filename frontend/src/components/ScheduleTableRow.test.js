import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import TeamTableRow  from './TeamTableRow'


describe("<TeamTableRow />", () => {
  const person = fromJS({
    uuid: 'person1',
    first_name: 'Firstname1',
    last_name: 'Lastname1',
    appointments:[
      {
        uuid: 'appt1',
        hours: '1.1',
        during: {}
      },
      {
        uuid: 'appt2',
        hours: '2.0',
        during: {}
      }
    ],
    actuals:[
      {
        uuid: 'actual1',
        hours: '1.1',
        during: {}
      },
      {
        uuid: 'actual2',
        hours: '2.0',
        during: {}
      }
    ]
  });

  it('renders a tr with 4 td', () => {
    const dataWrapper = shallow(<TeamTableRow person={person} />).find('tr > td')

    expect(dataWrapper).toHaveLength(4);
    expect(dataWrapper.at(0).text()).toEqual(person.get('last_name'))
    expect(dataWrapper.at(1).text()).toEqual(person.get('first_name'))
    expect(dataWrapper.at(2).text()).toEqual('3.1')
    expect(dataWrapper.at(3).text()).toEqual('3.1')
  });

});
