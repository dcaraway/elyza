import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { PulseLoader } from 'halogenium';

class ProjectDetail extends React.Component {
  static propTypes = {
    uuid: PropTypes.string.isRequired,
    loadProject: PropTypes.func.isRequired,
    project: ImmutablePropTypes.contains({
      uuid: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
    }),
    isFetching: PropTypes.bool.isRequired,
    workers: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
      }),
    ),
    appointments: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
      }),
    ),
    actuals: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
      }),
    ),
  };

  componentWillMount() {
    this.props.loadProject(this.props.uuid);
  }

  render() {
    const { isFetching, project, ...rest } = this.props;

    return (
      <div>
        { project && <ProjectView {...rest } /> }
        <PulseLoader loading={isFetching} color="#26A65B" size="16px" margin="4px" />
      </div>
    );
  }
}

const ProjectView = ({ project }) => (
  <div>
    <h2>{ project && project.get('title') }</h2>

    <h3>Team</h3>
    <ul>
      { renderAssignees(project.get('assignees')) }
    </ul>
  </div>
)

const renderAssignees = (assignees, workers) => {
  if(assignees && workers){
    return assignees.map(assignee => (
      <li key={ assignee }>
        {workers.get(assignee).get('last_name')},
        {workers.get(assignee).get('first_name')}
      </li>)
    );
  }

  return <li>No assignees</li>;
}

ProjectView.propTypes = {
  project: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }),
}

export default ProjectDetail
