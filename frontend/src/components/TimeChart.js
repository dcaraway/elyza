import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import {
  XYPlot,
  XAxis,
  YAxis,
  HorizontalGridLines,
  VerticalGridLines,
  Hint,
  MarkSeries,
  LineSeries
} from 'react-vis';
import "react-vis/dist/style.css";
import moment from "moment";

const toChartData = appointments => {
  let data = appointments.map(appointment =>
    (
      { x: moment(appointment.getIn(['during', 'lower'])).startOf().valueOf(),
        y: parseFloat(appointment.get('hours')) }
    )
  )

  console.log('toChartData created', data.toJS())

  return data.toJS()
}

export default class TimeChart extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      value: null
    };

    this._rememberValue = this._rememberValue.bind(this);
    this._forgetValue = this._forgetValue.bind(this);
  }

  _rememberValue(value) {
    this.setState({value});
  }

  _forgetValue() {
    this.setState({
      value: null
    });
  }

  _formatHint(dataPoint) {
    return [{
      title: moment(dataPoint.x).format('ddd, MMMM Do YYYY'),
      value: dataPoint.y
    }];
  }

  _renderLines(project){
    let workerAppointments = project.get('workerAppointments');

    return project.get('assignees').map(assignee => {
      let uuid = assignee.get('uuid');
      return (
        workerAppointments.has(uuid) && <LineSeries
          key={ uuid }
          data={ toChartData(workerAppointments.get(uuid)) } />
      )
    }
    )
  }

  _renderMarks(project){
    let workerAppointments = project.get('workerAppointments');

    return project.get('assignees').map(assignee => {
      let uuid = assignee.get('uuid');

      return (workerAppointments.has(assignee.get('uuid')) && <MarkSeries
        key={ assignee.get('uuid') }
        onValueMouseOver={this._rememberValue}
        onValueMouseOut={this._forgetValue}
        data={ toChartData(workerAppointments.get(uuid)) }/>
      )
    }
    )
  }

  render () {
    const { value } = this.state;
    const { project } = this.props;

    // <XAxis tickLabelAngle={-45} xDomain={[xLower.valueOf(), xUpper.valueOf()]} />
    // TODO organize the xDomain to be from earliest appointment to latest

    return (
      <XYPlot width={ 800 } height={ 300 } margin={{bottom: 70}} xType="time">
        <XAxis tickLabelAngle={-45} />
        <YAxis />
        <VerticalGridLines />
        <HorizontalGridLines />

        { project.has('assignees') && this._renderLines(project) }
        { project.has('assignees') &&this._renderMarks(project) }

        { value && <Hint value={ value } format={ this._formatHint }/> }
      </XYPlot>
    )
  }
}

TimeChart.propsTypes = {
  project: ImmutablePropTypes.contains({
    uuid: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
    budget_in_dollars: PropTypes.number.isRequired,
    assignees: ImmutablePropTypes.listOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
      })
    ),
    workerAppointments: ImmutablePropTypes.map,
  }).isRequired,
}
