import React from 'react';
import { shallow } from 'enzyme';
import { fromJS } from 'immutable';
import { ScheduleTable, NO_TEAM_MSG } from './ScheduleTable';
import ScheduleTableRow from './ScheduleTableRow';


describe("<ScheduleTable />", () => {
  const project = fromJS({
    uuid: 'someuuid',
    title: 'sometitle',
    budget_in_dollars: 0,
    assignees: [
      {uuid: 'assignee1',
        first_name: 'Firstname1',
        last_name: 'Lastname1',
      },

      {uuid: 'assignee2',
        first_name: 'Firstname2',
        last_name: 'Lastname2',
      },
    ],

    workerAppointments: {
      assignee1: [
        { uuid: 'appt1', hours: '1', during: {} },
        { uuid: 'appt12', hours: '12', during: {} }
      ],
      assignee2: [
        { uuid: 'appt2', hours: '2', during: {} }
      ]
    },

  });

  it('displays a message when there are no assignees', () => {
    const project = fromJS({
      'uuid': 'someuuid',
      'title': 'sometitle',
      'budget_in_dollars': 0,
    });

    const wrapper = shallow(<ScheduleTable project={project} />);
    expect(wrapper.find('p').text()).toEqual(NO_TEAM_MSG);
  });

  it('indicates when assignees is empty', () => {
    const project = fromJS({
      'uuid': 'someuuid',
      'title': 'sometitle',
      'budget_in_dollars': 0,
      'assignees': []
    });

    const wrapper = shallow(<ScheduleTable project={project} />);
    expect(wrapper.find('p').text()).toEqual(NO_TEAM_MSG);
  });

  it('displays a table', () => {
    const wrapper = shallow(<ScheduleTable project={project} />);

    expect(wrapper.find('Table')).toHaveLength(1);
  });

  it('displays table header with 5 labels', () => {
    const theadWrapper = shallow(<ScheduleTable project={project} />).find('Table > thead');

    expect(theadWrapper).toHaveLength(1);
    expect(theadWrapper.find('th')).toHaveLength(5);
  });

  it('displays a ScheduleTableRow for each assignee appointment', () => {
    const rowWrapper = shallow(<ScheduleTable project={project} />).find(ScheduleTableRow);

    expect(rowWrapper).toHaveLength(3);
    expect(rowWrapper.at(0)).toHaveProperty('person', project.get('assignees')[0])
    expect(rowWrapper.at(1)).toHaveProperty('person', project.get('assignees')[0])
    expect(rowWrapper.at(2)).toHaveProperty('person', project.get('assignees')[1])
  });

  it('displays ScheduleTableRow sorted by assignee last name', () => {
    const outOfOrder = fromJS([
      {
        'uuid': 'uuid2',
        'first_name': 'first',
        'last_name': 'last2'
      },
      {
        'uuid': 'uuid3',
        'first_name': 'first',
        'last_name': 'last3'
      },
      {
        'uuid': 'uuid1',
        'first_name': 'first',
        'last_name': 'last1'
      }
    ]);

    const unorderedProject = project.set('assignees', outOfOrder);

    const rowWrapper = shallow(<ScheduleTable project={ unorderedProject } />).find(ScheduleTableRow);

    expect(rowWrapper.at(0)).toHaveProperty('person', outOfOrder[2]);
    expect(rowWrapper.at(1)).toHaveProperty('person', outOfOrder[1]);
    expect(rowWrapper.at(2)).toHaveProperty('person', outOfOrder[0]);
  })

});
