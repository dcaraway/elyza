import React from 'react'
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';

class PersonDetail extends React.Component {
  static propTypes = {
    uuid: PropTypes.string.isRequired,
    loadPerson: PropTypes.func.isRequired,
    people: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        uuid: PropTypes.string.isRequired,
        first_name: PropTypes.string.isRequired,
        last_name: PropTypes.string.isRequired,
      }),
      PropTypes.string.isRequired,
    ),
    isFetching: PropTypes.bool.isRequired,
    error: ImmutablePropTypes.mapOf(
      ImmutablePropTypes.contains({
        name: PropTypes.string.isRequired,
        message: PropTypes.string.isRequired,
      }),
    ),
  };

  componentWillMount() {
    this.props.loadPerson(this.props.uuid)
  }

  render() {
    const { people, uuid } = this.props
    const first_name = people.getIn([uuid, 'first_name'])
    const last_name = people.getIn([uuid, 'last_name'])

    return (
      <div>
        <h2>{ `${first_name} ${last_name}` }</h2>
        <p>Stuff about this person goes here </p>
      </div>
    )
  }
}

export default PersonDetail
