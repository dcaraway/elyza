import React from 'react'
import { Link } from 'react-router-dom'
import { Nav, Navbar, NavItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'
import { logout } from '../utils/auth'

const Header = () => (
  <header>
    <nav>
      <Navbar>
        <Navbar.Header>
          <Navbar.Brand>
            <Link to='/'>Elyza</Link>
          </Navbar.Brand>
        </Navbar.Header>
        <Nav>
          <LinkContainer to="/people">
            <NavItem eventKey={1}>People</NavItem>
          </LinkContainer>
          <LinkContainer to="/projects">
            <NavItem eventKey={2}>Projects</NavItem>
          </LinkContainer>
          <NavItem eventKey={3} onClick={logout}>Logout</NavItem>
        </Nav>
      </Navbar>
    </nav>
  </header>)

export default Header
