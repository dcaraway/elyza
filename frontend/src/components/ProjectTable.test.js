import React from 'react';
import { shallow } from 'enzyme';

import ProjectTable from './ProjectTable';
import { fromJS, Map } from 'immutable';

describe("<ProjectTable />", () => {
  it('renders project row', () => {
    const projects = fromJS({
      'myid': {
        uuid: 'myid',
        title: 'mytitle',
      },
    });
    const basepath = '/foo';
    const wrapper = shallow(<ProjectTable projects={projects}
      basepath={ basepath } isFetching = { false } loadProject={()=>{}} />);

    expect(wrapper.find('div.project-list > ListGroup > ProjectRow')).toHaveLength(1);
  });

  it('displays message when no projects found', () => {
    const projects = new Map();
    const wrapper = shallow(<ProjectTable projects={projects}
    basepath={'doesntmatter'} isFetching = { false } loadProject={()=>{}} />);

    expect(wrapper.find('div.project-list > ListGroup').contains(<p>No projects found</p>)).toBeTruthy();
  });

  it('hides message when fetching', () => {
    const projects = new Map();
    const wrapper = shallow(<ProjectTable projects={projects}
    basepath = {'doesntmatter'} isFetching = { true } loadProject={()=>{}} />);

    expect(wrapper.find('div.project-list > ListGroup').contains(<p>No projects found</p>)).toBeFalsy();
  });
});
