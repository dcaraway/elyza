import React from 'react';
import PropTypes from 'prop-types';
import { ListGroupItem } from 'react-bootstrap'
import { LinkContainer } from 'react-router-bootstrap'

const ProjectRow = ({ title, path, onClick })=> (
  <LinkContainer to={path} >
    <ListGroupItem onClick={ onClick }>{title}</ListGroupItem>
  </LinkContainer>
);

ProjectRow.propTypes = {
  onClick: PropTypes.func.isRequired,
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default ProjectRow;
