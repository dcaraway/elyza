import { connect } from 'react-redux';
import { loadProjects, loadProject } from '../actions/';
import ProjectList from '../components/ProjectList';

const mapStateToProps = state => ({
  projects: state.getIn(['entities', 'projects']),
  isFetching: state.get('isFetching'),
})

const mapDispatchToProps = dispatch => ({
  loadProjects: () => dispatch(loadProjects()),
  loadProject: (uuid) => dispatch(loadProject(uuid))
})

const ProjectListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectList);

export default ProjectListContainer
