import { connect } from 'react-redux';
import { loadPerson, loadPeople } from '../actions/';
import PersonList from '../components/PersonList';

const mapStateToProps = state => ({
  people: state.getIn(['entities', 'workers']),
  isFetching: state.get('isFetching'),
})

const mapDispatchToProps = dispatch => ({
  loadPeople: () => dispatch(loadPeople()),
  loadPerson: uuid => dispatch(loadPerson(uuid)),
})

const PersonListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonList)

export default PersonListContainer
