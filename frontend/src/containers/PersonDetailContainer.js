import PersonDetail from '../components/PersonDetail'
import { loadPerson } from '../actions'
import { connect } from 'react-redux';

const mapStateToProps = (state, ownProps) => ({
  uuid: ownProps.match.params.uuid,
  people: state.getIn(['entities', 'workers']),
  isFetching: state.get('isFetching'),
  error: state.get('error'), //TODO get error particular to component
})

const mapDispatchToProps = dispatch => ({
  loadPerson: uuid => dispatch(loadPerson(uuid)),
})

const PersonDetailContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(PersonDetail)

export default PersonDetailContainer
