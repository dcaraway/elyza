import { connect } from 'react-redux'
import { loadProject } from '../actions/'
import ProjectDetail from '../components/ProjectDetail'
import { projectSchema } from '../utils/schema'
import { denormalize } from 'normalizr'

const denormalizeProject = (project, entities) => {
  const denormProject = denormalize(project, projectSchema, entities)

  if( !denormProject.has('assignees')) return denormProject

  const workerAppointments = denormProject.get('appointments')
    .groupBy(appt => appt.getIn(['worker', 'uuid']))

  const workerActuals = denormProject.get('actuals')
    .groupBy(actual => actual.getIn(['worker', 'uuid']))

  return denormProject.set('workerAppointments', workerAppointments).set('workerActuals', workerActuals)
}

const mapStateToProps = (state, ownProps) => {
  const uuid = ownProps.match.params.uuid;
  const isFetching = state.get('isFetching')
  let project = state.getIn(['entities', 'projects', uuid]);

  return {
    uuid,
    project: project && denormalizeProject(project, state.get('entities')),
    isFetching,
  };
};

const mapDispatchToProps = dispatch => ({
  loadProject: uuid => dispatch(loadProject(uuid))
})

const ProjectDetailContainer  = connect(
  mapStateToProps,
  mapDispatchToProps
)(ProjectDetail)

export default ProjectDetailContainer
