import os
import re
import json
from datetime import datetime
from collections import defaultdict
import dateutil.parser
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))


def _get_start_date(date):
    """For a date 'date', returns the first day \
            of the month
    """
    return date+relativedelta(day=1)


def _get_end_date(date):
    """
    For a date 'date' returns the end date for the month of 'date'.
    """
    return date + relativedelta(day=1, months=+1)


def _get_company(employee_id):
    if employee_id.lower().startswith('sc'):
        return 'unknown_subcontractor'
    return 'iai'


def datetime_handler(item):
    if isinstance(item, datetime):
        return item.isoformat()
    raise TypeError("Unknown type")


def load_data(workbook_path):
    assignment_data = _load_assignments(workbook_path)

    with open('assignments.json', 'w') as filepointer:
        print('Writing assignments to ', filepointer.name)
        filepointer.write(json.dumps(assignment_data, default=datetime_handler))


def _split_name(name):
    last_name, first_name = [parts.strip() for parts in name.split(',')]

    return last_name, first_name


def _collapse_task_id(task_id):
    return task_id.replace('-', '')


def _load_assignments(workbook_path):
    wb = load_workbook(workbook_path)
    ws = wb['Hours Archive']

    data = defaultdict(list)

    headers = tuple(col[0].value for col in ws.iter_cols(max_row=1))

    for row in ws.iter_rows(min_row=2):
        row_values = tuple(col.value for col in row)

        if not any(row_values):
            continue

        obj = {}
        last_name, first_name = _split_name(row_values[0])

        obj['first-name'] = first_name
        obj['last-name'] = last_name

        obj['lcat'] = row_values[1]
        obj['location'] = row_values[2]
        obj['employee-id'] = str(row_values[3]).lstrip('0')
        obj['task-name'] = row_values[4]
        obj['active-task-order'] = row_values[5]
        obj['contract'] = row_values[6]
        obj['task-order'] = row_values[7]

        if not int(obj['task-order']) > 0:
            raise ValueError('Bad project id: '+obj['task-order'])

        obj['nav-id'] = _collapse_task_id(row_values[8])

        if not re.match(r'\d{2,}', str(obj['nav-id']).replace('-', '')):
            raise ValueError('Bad nav id: '+obj['nav-id'])

        obj['company'] = _get_company(obj['employee-id'])

        assignments = []

        for month, hours in zip(headers[9:], row_values[9:]):
            if month and hours and isinstance(hours, (int, float)):
                start_date = _get_start_date(dateutil.parser.parse(month))
                end_date = _get_end_date(start_date)

                assignment_obj = {
                    'start-date': start_date,
                    'end-date': end_date,
                    'hours-total': round(hours, ndigits=1),
                }

                assignments.append(assignment_obj)

        obj['assignments'] = assignments
        data[obj['employee-id']].append(obj)

    return _transform_assignments(data)


def _transform_assignments(assignments_data):
    compiled_data = defaultdict(dict)

    for employee_id, assignments in assignments_data.items():
        for assignment in assignments:
            employee_data = compiled_data[employee_id]

            if not employee_data:
                employee_data.update(_basic_employee_info(assignment))

            matching_project = employee_data['projects'][assignment['nav-id']]

            if not matching_project:
                matching_project['nav-id'] = assignment['nav-id']
                matching_project['task-order'] = assignment['task-order']
                matching_project['assignments'] = assignment['assignments']
            else:
                print("duplicate assignments for {} {}: nav-id {}".format(
                    employee_data['first-name'],
                    employee_data['last_name'],
                    matching_project['nav-id']))
                matching_project['assignments'].extend(assignment['assignments'])

    return compiled_data


def _basic_employee_info(assignment):
    data = {}
    data['employee-id'] = str(assignment['employee-id']).lstrip('0')

    data['last-name'] = assignment['last-name']
    data['first-name'] = assignment['first-name']
    data['projects'] = defaultdict(dict)
    data['company'] = _get_company(assignment['employee-id'])

    return data


if __name__ == '__main__':
    import sys
    print('Extracting assignments from ', sys.argv[1])
    load_data(workbook_path=sys.argv[1])
    print('Extraction complete')
