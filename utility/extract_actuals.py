import os
import re
import json
from collections import defaultdict
from datetime import datetime
import dateutil.parser
from dateutil.relativedelta import relativedelta
from openpyxl import load_workbook
import py.path

CURRENT_PATH = os.path.dirname(os.path.abspath(__file__))

ACTUALS_FIELD_MAP = {
    'Job No_': 'nav-id',
    'Job WBS Code': 'task-id',
    'Labor Category': 'lcat',
    'Resource No._': 'employee-id',
    'Name': 'employee-name',
    'Hours': 'hours-total',
    'Labor Dollars Through G&A': 'labor-dollars',
    'Subcontract Labor Cost': 'labor-dollars',
    }


def _get_start_date(date):
    """For a date 'date', returns the first day \
            of the month
    """
    return date+relativedelta(day=1)


def _get_end_date(date):
    """
    For a date 'date' returns the first day of next month following 'date'.
    """
    return date + relativedelta(day=1, months=+1)


def _get_company(employee_id, last_name):
    if str(employee_id).lower().startswith('sc'):
        if '-' in last_name:
            return last_name.split('-')[0].strip().upper()
        else:
            return 'unknown_subcontractor'
    return 'iai'


def datetime_handler(item):
    if isinstance(item, datetime):
        return item.isoformat()

    raise TypeError("Unknown type")


def load_data(actuals_dir_path):
    actuals_data = _load_actuals(actuals_dir_path)
    with open('actuals.json', 'w') as filepointer:
        filepointer.write(json.dumps(actuals_data, default=datetime_handler))


def _load_actuals(actuals_dir_path):
    actuals = defaultdict(list)
    actuals_dir = py.path.local(actuals_dir_path)

    for wb_path in actuals_dir.visit('*abor*.xlsx'):
        if '~$' in wb_path.basename:
            continue

        wb = load_workbook(wb_path)

        for ws in wb.worksheets:
            data_date = dateutil.parser.parse(ws.title)
            start_date = _get_start_date(data_date)
            end_date = _get_end_date(data_date)

            headers = [col[0].value for col in ws.iter_cols(max_row=1)]

            # Log any headers that aren't found
            unmatched_headers = set([header for header in headers if not
                                     ACTUALS_FIELD_MAP.get(header)])

            matching_headers = set(headers) - unmatched_headers

            print("Headers in {} not found: {}".format(ws.title, unmatched_headers))

            for row in ws.iter_rows(min_row=2):
                row_data = dict(zip(headers, tuple(col.value for col in row)))

                if not row_data:
                    continue

                obj = {ACTUALS_FIELD_MAP[header]: row_data[header] for header in matching_headers}

                if float(obj['hours-total']) == 0:
                    continue

                obj['start-date'] = start_date
                obj['end-date'] = end_date
                obj['nav-id'] = str(obj['nav-id'] )
                obj['task-id'] = str(obj['task-id'] )
                obj['employee-id'] = str(obj['employee-id']).lstrip('0')

                actuals[obj['employee-id']].append(obj)

    return _transform_actuals(actuals)


def _transform_actuals(actuals_data):
    compiled_data = defaultdict(dict)

    for employee_id, actuals in actuals_data.items():
        for actual in actuals:
            employee_data = compiled_data[employee_id]

            if not employee_data:
                employee_data.update(_basic_employee_info(actual))

            matching_project = employee_data['projects'][actual['nav-id']]

            if not matching_project:
                matching_project['nav-id'] = actual['nav-id']

                task_order = re.match(r'(\d+)-.*', actual['task-id'])

                if task_order:
                    matching_project['task-order'] = task_order[1]

                matching_project['actuals'] = []

            task_obj = {
                    'lcat': actual['lcat'],
                    'task-id': actual['task-id'],
                    'hours-total': round(actual['hours-total'], ndigits=1),
                    'start-date': actual['start-date'],
                    'end-date': actual['end-date'],
                    'at-customer-site': _is_onsite(actual['lcat']),
                    }

            if actual.get('labor-dollars'):
                task_obj['labor-dollars'] = actual['labor-dollars']
                task_obj['hourly-rate'] = _get_rate(actual)

            matching_project['actuals'].append(task_obj)

    return compiled_data


def _is_onsite(lcat):
    return not lcat.lower().endswith('off')


def _basic_employee_info(actual):
    data = {}
    data['employee-id'] = actual['employee-id']

    last_name, first_name = _split_name(actual['employee-name'])

    data['last-name'] = last_name
    data['first-name'] = first_name
    data['projects'] = defaultdict(dict)
    data['company'] = _get_company(actual['employee-id'], actual['employee-name'])

    return data


def _split_name(name):
    last_name, first_name = [parts.strip() for parts in name.split(',')]

    # finance people used convention of putting
    if '-' in last_name:
        last_name = last_name.split('-')[1].strip()

    return last_name, first_name


def _get_rate(actual):
    if not actual['hours-total']:
        return 0.0

    rate = round(float(actual['labor-dollars'])/actual['hours-total'], 3)
    return rate


if __name__ == '__main__':
    import sys
    print('Extracting actuals from directory: ', sys.argv[1])
    load_data(actuals_dir_path=sys.argv[1])
    print('Extraction complete')
