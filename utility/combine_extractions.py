import json


def combine():
    with open('assignments.json') as f:
        assignments_data = json.loads(f.read())

    with open('actuals.json') as f:
        actuals_data = json.loads(f.read())

    with open('contracts.json') as f:
        contract_data = json.loads(f.read())

    # copy assignment data to actuals
    for employee_id, actual in actuals_data.items():
        assign_emp = assignments_data.get(employee_id)

        if not assign_emp:
            continue

        for project_id, project in actual['projects'].items():

            matching_assign_proj = assign_emp['projects'].get(project_id)

            if not matching_assign_proj:
                continue

            if 'assignments' not in project:
                project['assignments'] = matching_assign_proj['assignments']
            else:
                project['assignments'].extend(project)

            if matching_assign_proj.get('task-order'):
                project['task-order'] = matching_assign_proj['task-order']

    # if any employees in assignments not in actuals, add an entry to actuals
    unmatched = set(assignments_data.keys()) - set(actuals_data.keys())

    for employee_id in unmatched:
        actuals_data[employee_id] = assignments_data[employee_id]

    return {'workers': actuals_data, 'contracts': contract_data, }


if __name__ == '__main__':
    data = combine()
    with open('combined.json', 'w') as f:
        f.write(json.dumps(data))
