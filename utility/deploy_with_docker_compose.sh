#!/bin/bash

DOCKER_COMPOSE_FILENAME=production.yml
COMPOSE="docker-compose -f $DOCKER_COMPOSE_FILENAME"
DB_CONTAINER=postgres
API_CONTAINER=api

SCRIPT_DIR="$(dirname "$0")"
cd $SCRIPT_DIR/..

if ! [ -x "$(command -v docker-compose)" ]; then
  echo 'Error: docker-compose is not installed.' >&2
  exit 1
fi

if [ ! -r "$DOCKER_COMPOSE_FILENAME" ]; then
    echo 'There is no docker-compose file for production, did you change the file name?'
    exit 1;
fi

echo 'Pull images'
eval "$COMPOSE pull"
if [ $? -ne 0 ]; then
    echo 'pull failed'
    exit 1;
fi

echo 'Backing up the database'
eval "$COMPOSE run $DB_CONTAINER /usr/local/bin/backup"
if [ $? -ne 0 ]; then
    echo 'Backup failed'
    exit 1;
fi

echo 'Creating new containers'
eval "$COMPOSE create"

echo 'Running migrations'
eval "$COMPOSE run $API_CONTAINER python manage.py migrate"
if [ $? -ne 0 ]; then
    echo 'Migrations failed'
    exit 1;
fi

echo 'Restarting any rebuilt services'
eval "$COMPOSE start"
