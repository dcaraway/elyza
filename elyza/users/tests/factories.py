import factory
from allauth.account.models import EmailAddress


class EmailFactory(factory.django.DjangoModelFactory):
    email = factory.Sequence(lambda n: 'someemail-{0}@foo.bar'.format(n))
    verified = True
    primary = True

    class Meta:
        model = EmailAddress


class UserFactory(factory.django.DjangoModelFactory):
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    username = factory.Sequence(lambda n: 'user-{0}'.format(n))
    email = factory.Sequence(lambda n: 'user-{0}@example.com'.format(n))
    password = factory.PostGenerationMethodCall('set_password', 'password')

    class Meta:
        model = 'users.User'
        django_get_or_create = ('username', )

    email_address = factory.RelatedFactory(EmailFactory, 'user')
