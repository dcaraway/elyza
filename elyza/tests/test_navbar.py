from test_plus.test import TestCase
from django.urls import reverse

class TestNavBar(TestCase):
    """
    Integration tests for the navbar
    """

    def test_signup_shows_when_unauthenticated(self):
        response = self.get('account_login')
        assert "sign-up-link" in str(response.content)

    def test_signup_hidden_on_signup_page(self):
        response = self.get('account_signup')
        assert "sign-up-link" not in str(response.content)

    def test_signup_hidden_when_authenticated(self):
        user = self.make_user()

        with self.login(username=user.username, password='password'):
            response = self.get('utilization:worker-list')
            assert "sign-up-link" not in str(response.content)

    def test_login_shows_when_unauthenticated(self):
        response = self.get('account_signup')
        assert "log-in-link" in str(response.content)

    def test_login_hidden_on_login_page(self):
        response = self.get('account_login')
        assert "log-in-link" not in str(response.content)

    def test_login_hidden_when_authenticated(self):
        user = self.make_user()

        with self.login(username=user.username, password='password'):
            response = self.get('utilization:worker-list')
            assert "log-in-link" not in str(response.content)

    def test_admin_shows_for_superuser(self):
        user = self.make_user()

        user.is_superuser = True
        user.save()

        with self.login(username=user.username, password='password'):
            response = self.get('utilization:worker-list')
            assert "admin-dash-link" in str(response.content)

    def test_admin_hidden_from_regular_user(self):
        user = self.make_user()

        with self.login(username=user.username, password='password'):
            response = self.get('utilization:worker-list')
            assert "admin-dash-link" not in str(response.content)
