from test_plus.test import TestCase
from django.test import override_settings
from django.urls import reverse


@override_settings(DEBUG=False, GOOGLE_ANALYTICS_PROPERTY_ID='someid')
class TestGoogleAnalytics(TestCase):

    def setUp(self):
        self.user = self.make_user('u1', perms=['utilization.view_worker'])

    def test_analytics_enabled_by_env(self):
        with self.login(self.user):
            response = self.get('utilization:worker-list')
            assert "analytics.js" in str(response.content)
            assert "someid" in str(response.content)

    def test_analytics_disabled_for_superuser(self):
        self.user.is_superuser = True
        self.user.save()

        with self.login(self.user):
            response = self.get('utilization:worker-list')
            assert "analytics.js" not in str(response.content)
