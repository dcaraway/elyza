"""Verify the correct configuration of DNS in production

"""
from unittest import TestCase
import requests
import pytest


@pytest.mark.smoketest
class RoutingSuite(TestCase):
    """Tests to verify correct routing in production
    """

    def test_domain_forces_https(self):
        """All http traffic should redirect to https

        """
        response = requests.get('http://elyza.co')

        self.assertEqual('https://elyza.co/', response.url)
        self.assertEqual(200, response.status_code)
        self.assertTrue(response.history)
        self.assertEqual(301, response.history[-1].status_code)

    def test_website_redirects_with_uri(self):
        """Redirected http traffic should retain url including query string
        """
        response = requests.get('http://elyza.co/foo/bar?biv=42')

        self.assertEqual('https://elyza.co/foo/bar?biv=42', response.url)

    def test_former_website_redirects(self):
        """Originally started with elyza domain. Make sure it's redirected to new site
        """
        response = requests.get('http://elyza.co/foo/bar?biv=42')

        self.assertEqual('https://elyza.co/foo/bar?biv=42', response.url)
