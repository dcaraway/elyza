"""Dummy resumes to use for seeding the database.
"""
import os
import re
import json

HERE = os.path.dirname(__file__)
RESUMES_JSON_FILE = os.path.join(HERE, 'resumes.json')


def convert_resumes():
    """Converts text file of resumes into a json of them.
    Expects a line of just ~ characters to delimit the resumes
    """
    RESUMES_FILE = os.path.join(HERE, 'resumes.txt')

    RESUME_TEXT = open(RESUMES_FILE, 'r').read()
    SPLIT_RESUMES = re.split('~{2,}', RESUME_TEXT)
    RESUMES = [resume.strip() for resume in SPLIT_RESUMES]

    with open(RESUMES_JSON_FILE, 'w') as f:
        f.write(json.dumps(RESUMES))


def load_resumes():
    """Reads resume json in and parses it, returning the list of resumes

    Returns:
        a list of resumes
    """
    with open(RESUMES_JSON_FILE, 'r') as f:
        return json.loads(f.read())


RESUMES = load_resumes()

if __name__ == '__main__':
    convert_resumes()
