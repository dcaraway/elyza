"""Serializers determine the way that the Django Rest Framework
represents data in API responses.

"""
from rest_framework import serializers
from rest_framework.relations import SlugRelatedField
from drf_extra_fields.fields import DateRangeField
from .models import (Project, Assignment, Worker, Appointment, Actual,)


class EagerLoadingMixin:
    """Mixin will read `_SELECT_RELATED_FIELDS` and `_PREFETCH_RELATED_FIELDS`
    class attributes"""
    @classmethod
    def setup_eager_loading(cls, queryset):
        if hasattr(cls, "_SELECT_RELATED_FIELDS"):
            queryset = queryset.select_related(*cls._SELECT_RELATED_FIELDS)
        if hasattr(cls, "_PREFETCH_RELATED_FIELDS"):
            queryset = queryset.prefetch_related(*cls._PREFETCH_RELATED_FIELDS)
        return queryset


class UUIDRelatedField(SlugRelatedField):
    """
    Relates to another model using a uuid slug

    Args:
        model (Class): the class of the model related to
        slug_field (str): Optional, the field name on the related model to use for identity
    """
    DEFAULT_FIELD_NAME = 'uuid'

    def __init__(self, model=None, slug_field=None, **kwargs):
        slug = slug_field if slug_field else self.DEFAULT_FIELD_NAME

        if model:
            queryset = model.objects.all()
            super(UUIDRelatedField, self).__init__(queryset=queryset, slug_field=slug, **kwargs)
        else:
            # For read-only
            super(UUIDRelatedField, self).__init__(slug_field=slug, **kwargs)


# pylint: disable=no-member
class AppointmentSerializer(serializers.ModelSerializer):
    """Serializes workers
    """
    worker = UUIDRelatedField(model=Worker)
    project = UUIDRelatedField(model=Project)
    during = DateRangeField()

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        model = Appointment
        fields = ('uuid', 'hours', 'during', 'project', 'worker')
        read_only = ('uuid', )


class ActualSerializer(serializers.ModelSerializer):
    """Serializes actuals
    """
    worker = UUIDRelatedField(model=Worker)
    project = UUIDRelatedField(model=Project)
    during = DateRangeField()

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        model = Actual
        fields = ('uuid', 'hours', 'during', 'project', 'worker')
        read_only = ('uuid', )


# pylint: disable=no-member
class WorkerSerializer(serializers.ModelSerializer):
    """Serializes workers
    """

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        model = Worker
        fields = ('uuid', 'first_name', 'last_name',)
        read_only = ('uuid', )


class ProjectPartialSerializer(serializers.ModelSerializer):
    """Serializes projects for a list view, does not include relational fields
    """

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        model = Project
        fields = ('uuid', 'title', 'budget_in_dollars',)
        read_only = ('uuid', )


class ProjectSerializer(serializers.ModelSerializer, EagerLoadingMixin):
    """Serializes projects
    """
    assignees = WorkerSerializer(many=True, read_only=True)
    appointments = AppointmentSerializer(many=True, read_only=True)
    actuals = ActualSerializer(many=True, read_only=True)

    # Eager loading
    _PREFETCH_RELATED_FIELDS = ['assignees', 'appointments', 'actuals']

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        model = Project
        fields = ('uuid', 'title', 'budget_in_dollars', 'assignees',
                  'appointments', 'actuals')
        read_only = ('uuid', )


# pylint: disable=no-member
class AssignmentSerializer(serializers.ModelSerializer):
    """Serializes assignments
    """

    # TODO Can we look up the model on the field using the declared model in Meta?
    worker = UUIDRelatedField(model=Worker)
    project = UUIDRelatedField(model=Project)

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        model = Assignment
        fields = ('uuid', 'project', 'worker',)
        read_only = ('uuid', )
