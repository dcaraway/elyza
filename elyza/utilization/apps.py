from django.apps import AppConfig


class UtilizationConfig(AppConfig):
    name = 'elyza.utilization'
    verbose_name = 'Utilization'
