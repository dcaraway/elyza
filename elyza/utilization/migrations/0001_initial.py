# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-11-05 00:31
from __future__ import unicode_literals

from django.conf import settings
import django.contrib.postgres.fields.ranges
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields
import shortuuidfield.fields
from django.contrib.postgres.operations import CreateExtension

class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Actual',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
                ('hours', models.DecimalField(decimal_places=1, max_digits=5)),
                ('task_id', models.CharField(max_length=20, null=True)),
                ('during', django.contrib.postgres.fields.ranges.DateRangeField()),
                ('billing_dollars', models.DecimalField(decimal_places=2, max_digits=10, null=True)),
            ],
            options={
                'db_table': 'utilization_actual',
            },
        ),
        migrations.CreateModel(
            name='Appointment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
                ('hours', models.DecimalField(decimal_places=1, default=0, max_digits=5)),
                ('during', django.contrib.postgres.fields.ranges.DateRangeField()),
            ],
            options={
                'db_table': 'utilization_appointment',
            },
        ),
        migrations.CreateModel(
            name='Assignment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
            ],
            options={
                'db_table': 'utilization_assignment',
            },
        ),
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('name', models.CharField(max_length=60)),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
            ],
            options={
                'db_table': 'utilization_company',
            },
        ),
        migrations.CreateModel(
            name='Contract',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from='title', verbose_name='slug')),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
                ('during', django.contrib.postgres.fields.ranges.DateRangeField()),
                ('accounting_id', models.CharField(max_length=20, unique=True)),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='utilization.Contract')),
            ],
            options={
                'db_table': 'utilization_contract',
            },
        ),
        migrations.CreateModel(
            name='LaborCategory',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from='title', verbose_name='slug')),
                ('code', models.CharField(max_length=20, unique=True)),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
                ('at_customer_site', models.NullBooleanField()),
            ],
            options={
                'db_table': 'utilization_labor_category',
            },
        ),
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('title', models.CharField(max_length=255, verbose_name='title')),
                ('description', models.TextField(blank=True, null=True, verbose_name='description')),
                ('slug', django_extensions.db.fields.AutoSlugField(blank=True, editable=False, populate_from='title', verbose_name='slug')),
                ('budget_in_dollars', models.PositiveIntegerField(default=0)),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
            ],
            options={
                'db_table': 'utilization_project',
            },
        ),
        migrations.CreateModel(
            name='Worker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('first_name', models.CharField(max_length=30)),
                ('last_name', models.CharField(max_length=30)),
                ('uuid', shortuuidfield.fields.ShortUUIDField(blank=True, editable=False, max_length=22)),
                ('resume', models.TextField()),
                ('accounting_id', models.CharField(max_length=20, unique=True)),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utilization.Company')),
                ('projects', models.ManyToManyField(through='utilization.Assignment', to='utilization.Project')),
                ('user', models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'db_table': 'utilization_worker',
                'ordering': ['last_name'],
            },
        ),
        migrations.AddField(
            model_name='project',
            name='assignees',
            field=models.ManyToManyField(through='utilization.Assignment', to='utilization.Worker'),
        ),
        migrations.AddField(
            model_name='contract',
            name='project',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to='utilization.Project'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='project',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utilization.Project'),
        ),
        migrations.AddField(
            model_name='assignment',
            name='worker',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utilization.Worker'),
        ),
        migrations.AddField(
            model_name='appointment',
            name='assignment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utilization.Assignment'),
        ),
        migrations.AddField(
            model_name='actual',
            name='assignment',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utilization.Assignment'),
        ),
        migrations.AddField(
            model_name='actual',
            name='lcat',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='utilization.LaborCategory'),
        ),
        CreateExtension('btree_gist'),
        migrations.RunSQL("ALTER TABLE utilization_actual ADD EXCLUDE USING gist(assignment_id WITH =, task_id WITH =, lcat_id WITH=, during WITH &&);"),
        migrations.RunSQL("ALTER TABLE utilization_appointment ADD EXCLUDE USING gist(assignment_id WITH =, during WITH &&);"),
    ]
