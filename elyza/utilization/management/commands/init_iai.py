import json
from django.core.management.base import BaseCommand
from django.db import transaction
import py.path
import re
from elyza.utilization.models import (Company, Worker, Project,
                                      Appointment, Actual,
                                      Contract, Assignment, LaborCategory,
                                      User,)
from django.core.exceptions import ObjectDoesNotExist
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import date


def _remove_leading_zeros(text):
    return re.match(r'^0*(.*)$', text)[1]


class Command(BaseCommand):
    """Loads the initial IAI actuals and assignment data
    """

    def add_arguments(self, parser):
        parser.add_argument('data-path', type=py.path.local)

    @transaction.atomic()
    def handle(self, *args, **options):
        financial_data = json.loads(options['data-path'].read())

        # Create contracts
        for contract in financial_data['contracts'].values():
            print('Creating contract and project for: ({}): {}'.format(contract['project-id'], contract['title']))
            project = Project(title=contract['title'])
            project.save()

            contract_obj = Contract(project=project)
            contract_obj.accounting_id = _remove_leading_zeros(contract['project-id'])
            task_order_id = contract.get('task-order')

            if task_order_id:
                contract_obj.task_order_id = task_order_id

            contract_obj.classification = contract['contract-type']

            start_date = parser.parse(contract['start-date']).date()
            end_date = parser.parse(contract['end-date']).date()
            contract_obj.during = (start_date, end_date)

            contract_obj.award_dollars = int(float(contract['funded-dollars'].replace('$', '').replace(',', '')))
            contract_obj.save()

        for employee_id, worker_obj in financial_data['workers'].items():
            # Create a new company if it doesn't exist, else get the company
            try:
                company = Company.objects.get(name__iexact=worker_obj['company'])
            except ObjectDoesNotExist:
                print('Creating new company: ', worker_obj['company'])
                company = Company(name=worker_obj['company'])
                company.save()

            strp_employee_id = employee_id.lstrip('0')

            try:
                worker = Worker.objects.get(accounting_id=strp_employee_id)
            except ObjectDoesNotExist:
                worker = Worker()
                worker.accounting_id = strp_employee_id
                worker.first_name = worker_obj['first-name']
                worker.last_name = worker_obj['last-name']
                worker.company = company

                email = worker_obj.get('email')
                if email:
                    user = User(email=email, username=email.split('@')[0])
                    user.set_unusable_password()
                    user.save()
                    worker.user = user

                worker.save()

            for nav_id, project_obj in worker_obj['projects'].items():
                # Create new contract if it doesn't exist, else get the contract
                corrected_nav_id = _remove_leading_zeros(nav_id)
                try:
                    contract = Contract.objects.get(accounting_id__iexact=corrected_nav_id)
                    project = contract.project
                except ObjectDoesNotExist:
                    print('Creating new contract and project for: ', corrected_nav_id)
                    project = Project()
                    project.save()

                    contract = Contract(project=project, accounting_id=corrected_nav_id)
                    contract.during = (date.today(), date.today() + relativedelta(days=1))
                    contract.save()

                # Create new Assignment if it doesn't exist, else get it
                try:
                    proj_assign = Assignment.objects.get(project=project,
                                                         worker=worker)
                except ObjectDoesNotExist:
                    proj_assign = Assignment(project=project, worker=worker)
                    proj_assign.save()

                for actual in project_obj.get('actuals', []):

                    # Create new LCAT if one does not exist
                    try:
                        lcat = LaborCategory.objects.get(code=actual['lcat'])
                    except:
                        print('Creating a new LCAT: ', actual['lcat'])
                        lcat = LaborCategory(code=actual['lcat'])
                        lcat.at_customer_site = actual['at-customer-site']
                        lcat.save()

                    ac = Actual()
                    ac.hours = actual['hours-total']

                    start_date = parser.parse(actual['start-date']).date()
                    end_date = parser.parse(actual['end-date']).date()

                    ac.during = (start_date, end_date)
                    ac.task_id = actual['task-id']
                    ac.assignment = proj_assign
                    ac.lcat = lcat
                    ac.billing_dollars = actual.get('labor-dollars')

                    ac.save()

                for assignment in project_obj.get('assignments', []):
                    asgn = Appointment()

                    start_date = parser.parse(assignment['start-date']).date()
                    end_date = parser.parse(assignment['end-date']).date()
                    asgn.during = (start_date, end_date)

                    asgn.hours = assignment['hours-total']
                    asgn.assignment = proj_assign
                    asgn.save()

            worker.save()
