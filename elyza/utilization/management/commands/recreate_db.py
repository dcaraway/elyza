from django.core import management
from django.core.management.base import BaseCommand
import logging

class Command(BaseCommand):
    """Convenience method to reset the database, migrate it and reseed it
    """
    help = "Resets the database, then runs migrations and seeds the database for this project."

    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument(
            '--noinput', action='store_false',
            dest='interactive', default=True,
            help='Tells Django to NOT prompt the user for input of any kind.')

    def handle(self, *args, **options):
        if options.get('interactive'):
            confirm = input("""
You have requested database recreation.
This will IRREVERSIBLY DESTROY
ALL data in the database.
Are you sure you want to do this?
Type 'yes' to continue, or 'no' to cancel: """)
        else:
            confirm = 'yes'

        if confirm != 'yes':
            print("Recreation cancelled.")
            return

        logging.info("Resetting all database tables")
        management.call_command('reset_db', interactive=False)

        logging.info("Running database migrations")
        management.call_command('migrate')

        logging.info("Seeding the database with data")
        management.call_command('seed')