import json
from django.core.management.base import BaseCommand
from django.db import transaction
import py.path
import re
from elyza.utilization.models import (Company, Worker, Project,
                                      Appointment, Actual,
                                      Contract, Assignment, LaborCategory,
                                      User,)
from django.core.exceptions import ObjectDoesNotExist
from dateutil import parser
from dateutil.relativedelta import relativedelta
from datetime import date
from django.contrib.auth.models import Group


def _remove_leading_zeros(text):
    return re.match(r'^0*(.*)$', text)[1]


class Command(BaseCommand):
    """Loads the initial IAI actuals and assignment data
    """

    def add_arguments(self, parser):
        parser.add_argument('data-path', type=py.path.local)

    @transaction.atomic()
    def handle(self, *args, **options):
        pmo = json.loads(options['data-path'].read())

        g = Group.objects.get_or_create(name='pmo')[0]

        for pm in pmo:
            #Look up the Worker for pm
            worker = Worker.objects.get(last_name__iexact=pm['last-name'], first_name__istartswith=pm['first-name'][0:2])
            #Create user for that PM with no password
            username = "{firstletter}{lastname}".format(firstletter=pm['first-name'][0], lastname=pm['last-name'])
            user = User(email=pm['email'], first_name=pm['first-name'], last_name=pm['last-name'],
                username=username.lower())
            user.set_unusable_password()

            user.save()

            worker.user = user
            worker.save()

            #Add user to the pmo group
            user.groups.add(g)
            user.save()