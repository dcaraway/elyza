from datetime import date
import random
from dateutil.relativedelta import relativedelta
from django.core.management.base import BaseCommand
from django.db import transaction
from elyza.users.models import User
from elyza.users.tests.factories import UserFactory
from elyza.utilization.tests.factories import (CompanyFactory, WorkerFactory,
                                               ContractFactory, ProjectFactory,
                                               AssignmentFactory, ActualFactory,)
from elyza.utilization.services import generate_appointments

DAYS_PER_YEAR = 365
NUM_TASK_ORDERS = 25
NUM_PEOPLE_PRIME = 350
NUM_PROPOSAL_WORKERS = 5
NUM_SUBCONTRACTORS = 5
NUMBER_OF_SUBCONTRACTOR_WORKERS = 50
PRIME_NAME = "primecompany"
ADMIN_USER = "test"
ADMIN_PASSWORD = "password"
ADMIN_EMAIL = "test@test.com"
DB_HAS_USERS_ERROR_MESSAGE = "Database is not empty. Please run flush or reset_db."
MONTHLY_WORK_HOURS = 155


class Command(BaseCommand):
    """Seeds the database
    """

    def add_arguments(self, parser):
        pass

    @transaction.atomic()
    def handle(self, *args, **options):
        if User.objects.exists():
            raise Exception(DB_HAS_USERS_ERROR_MESSAGE)

        seed_database()


def seed_database(num_task_orders=NUM_TASK_ORDERS,
                  num_people_prime=NUM_PEOPLE_PRIME,
                  num_people_sub=NUMBER_OF_SUBCONTRACTOR_WORKERS, num_subs=NUM_SUBCONTRACTORS):
    # Create administrator account
    UserFactory(username=ADMIN_USER, email=ADMIN_EMAIL, password=ADMIN_PASSWORD,
                is_superuser=True, is_staff=True)

    # Create the prime contractor
    company = CompanyFactory(name=PRIME_NAME)

    # Create subcontractors
    subcompanies = CompanyFactory.create_batch(num_subs)

    # Create the 5 year IDIQ contract
    idiq = create_idiq()

    # Create a pool of task orders for the contract
    task_orders = ContractFactory.create_batch(num_task_orders, parent=idiq)

    # Create a proposal project
    proposal_project = ProjectFactory.create(title='Hope we win!')

    # Create employees for prime contractor
    employees = _create_company_workers(company, num_people_prime)

    # Create employees for each subcontractor
    subcontract_employees = [worker for sub in subcompanies
                             for worker in _create_company_workers(sub, num_people_sub)]

    workers = employees + subcontract_employees

    # assign workers to projects
    populate_task_orders(workers=workers, task_orders=task_orders)

    # Assign random workers (prime or subcontractors) to proposal project
    for worker in random.sample(workers, NUM_PROPOSAL_WORKERS):
        AssignmentFactory(worker=worker, project=proposal_project)


def create_idiq():
    """
    Create the parent 5 year indefinite-quantity, indefinite-delivery contract vehicle.
    """
    start = date.today() - relativedelta(years=3, day=1)
    end = start + relativedelta(years=5, day=1)
    during = (start, end)

    return ContractFactory.create(during=during)


def _create_company_workers(company, number_of_workers):
    """Creates and returns workers for a given company.\
    Workers are created and stored in database using factory methods.

    Args:
        company (Company): a company
        number_of_workers (int): number of Worker to create for company

    Returns:
        the list of workers created for a company
    """
    return WorkerFactory.create_batch(number_of_workers, company=company)


def populate_task_orders(workers, task_orders):
    """Helper function: will assign workers to task orders and
    generate their appointments and actuals

    Args:
        workers (List<Worker>): workers to assign to task orders. A worker will have at least 1 and up to 3 task orders
        task_orders (List<Contract>): task orders to assign workers to
    """
    if len(set(task_orders)) < 3:
        raise Exception("at least 3 task orders required")

    for worker in workers:
        # Assign workers up to 3 projects
        assignments = [AssignmentFactory(worker=worker, project=task_order.project)
                       for task_order in random.sample(task_orders, random.randint(1, 3))]

        appointments = generate_appointments(assignments)

        # select appointments that occurred before today
        past_appts = [appt for appt in appointments if appt.during.end < date.today()]

        # for each past appt, generate an actual with random hour
        for appt in past_appts:
            # Generate random hours between 90% and 110% of appt hours
            hours = random.randint(round(appt.hours * 0.9)*100,
                                   round(appt.hours * 1.1)*100) / 100

            ActualFactory(hours=hours, during=appt.during,
                          worker=appt.worker, project=appt.project)
