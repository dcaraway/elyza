from datetime import datetime
from collections import namedtuple, defaultdict
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
from numpy import busday_count
from ortools.linear_solver import pywraplp
from elyza.utilization.tests.factories import (AppointmentFactory)
from elyza.utilization.models import Contract

DAILY_WORK_HOURS = 8
MIXED_WORKERS_ERROR_MESSAGE = "Assignments contain multiple workers"
ASSIGNMENT_HAS_APPOINTMENTS_ERROR_MESSAGE = "Assignments has existing appointments"
MaxHoursEntry = namedtuple('MaxHoursEntry', ['hours', 'during', 'contract_id'])
DurationSegment = namedtuple("DurationSegment", field_names=["start", "end"])
SEGMENT_KEY_FORMAT = '%Y%m'


def generate_appointments(assignments):
    """Given a worker's assignments, generate appointments for each month for each assignment
    such that the maximum daily hours are not exceeded and hours are evenly distributed among
    active projects. This function makes a lot of totally inaccurate assumptions but is a good
    start.

    TODO move this to some kind of scheduling service

    Assumptions (TODO these are not valid):
        * worker work 8 hours per day
        * worker only works on weekdays
        * worker does not use vacation, sickleave or take time off for holidays
        * worker should divide time equally among all active assignments

    Args:
        assignments (Assignment): all the assignments for a worker

    Returns:
        a list of appointments that have been created
    """
    if len({assignment.worker for assignment in assignments}) > 1:
        raise Exception(MIXED_WORKERS_ERROR_MESSAGE)

    if assignments[0].worker.appointments.count() > 0:
        raise Exception(ASSIGNMENT_HAS_APPOINTMENTS_ERROR_MESSAGE)

    # Dictionary to contain the work hours. We index the data by
    # string start - end date so that  later we can look through
    # this data to determine the optimum mix for each duration. Note that
    # this will only work if we combine by month, otherwise, need code changes
    workhour_dict = defaultdict(list)
    appointments = []

    for assignment in assignments:
        for segment in monthly_workhours(assignment.project.contract):
            key = segment.during.start.strftime(SEGMENT_KEY_FORMAT)
            workhour_dict[key].append(segment)

    for workhours in workhour_dict.values():
        for segment in optimize(workhours):
            # pylint: disable=no-member
            contract = Contract.objects.get(id=segment.contract_id)

            appointments.append(
                AppointmentFactory(
                    worker=assignments[0].worker,
                    project=contract.project,
                    hours=segment.hours,
                    during=segment.during))

    return appointments


def _use_simple_division(workhours, max_workhours):
    """In situation where worker is working the whole month on all projects,
    evenly distribute hours between the projects"""
    hours = [workhour.hours for workhour in workhours]
    return len(set(hours)) == 1 and hours[0] == max_workhours


def optimize(workhours):
    """We have multiple workhours occurring at the same time, so
    have to adjust based on constraints. We use the excellent Google
    Organizational Research algorithms for linear programming for this
    task

    Args:
        workhours (tuple): a list of workhours

    Returns:
        a list of tuples of workhours with optimized hour entries
    """
    if len(workhours) == 1:
        return workhours

    # beginning of month
    # TODO start of month and max workhours may already be in the workhours entry
    # check for that before recalculating
    begin_date = workhours[0].during.start - relativedelta(day=1)
    end_date = begin_date + relativedelta(months=1)
    max_workhours = busday_count(begindates=begin_date, enddates=end_date).item() * DAILY_WORK_HOURS

    if _use_simple_division(workhours, max_workhours):
        return [workhour._replace(hours=workhour.hours / len(workhours)) for workhour in workhours]

    solver = pywraplp.Solver('Workhour Resolver', pywraplp.Solver.GLOP_LINEAR_PROGRAMMING)
    variables = []

    # Represents what we're trying to maximize, in this case, billable hours
    # Form is x1+x2+x3...
    objective = solver.Objective()
    objective.SetMaximization()

    # Represents what's the upper limit. In this case, there's only so many work days
    # and we can only work 8 hours each day, so that's the upper limit
    # form is 0 <= x1 + x2 + x3... <= max_workhours
    constraint = solver.Constraint(0, max_workhours, 'max_hours_no_overtime')

    variables = []

    for idx, val in enumerate(workhours):
        # Create constrained variables
        var = solver.NumVar(0, val.hours, "x{}".format(idx))

        # add variable to the objective (what we want to maximize) and
        # constraint (what limits us). In this case, we want to maximize
        # hours worked monthly but we're limited by the fact that we're
        # not allowing overtime so upper limite is max work hours in month
        objective.SetCoefficient(var, 1)
        constraint.SetCoefficient(var, 1)

        variables.append(var)

    # invoke the solver
    solver.Solve()

    # update the hours
    return [workhours[idx]._replace(hours=variable.solution_value())
            for idx, variable in enumerate(variables)]


def monthly_workhours(contract):
    """Work hours generator for a contract by determing
    max work hours for each month from contract start to contract finish. Hours
    are returned as MaxHourEntry named tuples, representing the normal amount
    of hours a worker would be scheduled to work assuming an 8 hour day, no
    holidays, no vacation and no other assignments

    Args:
        contract (Contract): a contract to generate monthly workhours for

    Returns:
        a work hour generator
    """
    for interval in to_monthly_intervals(start=contract.during.lower, end=contract.during.upper):
        workdays = busday_count(begindates=interval.start, enddates=interval.end).item()
        yield MaxHoursEntry(hours=workdays * DAILY_WORK_HOURS, during=interval, contract_id=contract.id)


def to_monthly_intervals(start, end):
    """Generator that divides a period defined by a start and end into repeating schedule, starting
    on start, recurring each month on the 1st of the month and ending on end.

    Args:
        start (date): first day of iteration (inclusive)
        end (date): the last day of iteration (exclusive). iterations will include up to one day before end.

    Returns:
        generator of DurationSegment ordered from oldest to newest start date
    """
    # If midmonth start
    if start.day > 1:
        start_of_next_month = start + relativedelta(months=1, day=1)
        yield DurationSegment(start=start, end=start_of_next_month)

    # convert start and end dates to datetimes at midnight
    start_dt = datetime.combine(start, datetime.min.time())

    # midnight of the day before end
    end_dt = end - relativedelta(days=1, hour=0, minute=0)

    # From start_dt until (exclusive) end_dt, on the first of each month
    for rr in rrule(MONTHLY, dtstart=start_dt, until=end_dt, bymonthday=1):
        segment_start = rr.date()
        first_of_next_month = segment_start + relativedelta(months=1, day=1)
        segment_end = first_of_next_month if first_of_next_month < end else end

        yield DurationSegment(start=segment_start, end=segment_end)
