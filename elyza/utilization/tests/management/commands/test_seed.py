from unittest.mock import patch
from test_plus.test import TestCase
import pytest
from dateutil.relativedelta import relativedelta
from django.db.models import Count
from elyza.users.tests.factories import UserFactory
from elyza.utilization.tests.factories import (WorkerFactory, ContractFactory)
from elyza.utilization.management.commands import seed
from elyza.users.models import User
from elyza.utilization.models import (Company, Contract, Worker, Assignment, Project)

NUM_TASK_ORDERS = 5
NUM_PEOPLE_PRIME = 2
NUM_PEOPLE_SUB = 3


# pylint: disable=no-self-use
class TestCommand(TestCase):
    """
    Tests for "seed" command
    """

    def test_handle_raises_when_users_exist(self):
        """
        Test that handle() method raises exception when a User exists
        """
        UserFactory.create()

        with pytest.raises(Exception) as excinfo:
            seed.Command().handle()

        assert seed.DB_HAS_USERS_ERROR_MESSAGE in str(excinfo.value)

    def test_handle_calls_seed_database(self):
        """
        Test that handle() method invokes seed_database
        """
        with patch.object(seed, 'seed_database') as mock_seed_database:
            seed.Command().handle()
        mock_seed_database.assert_called_once()


# pylint: disable=no-member
class TestSeedDatabase(TestCase):
    """
    Tests for the seed_database() function
    """

    @classmethod
    def setUpTestData(cls):
        seed.seed_database(num_task_orders=NUM_TASK_ORDERS,
                           num_people_prime=NUM_PEOPLE_PRIME,
                           num_people_sub=NUM_PEOPLE_SUB)

    def setUp(self):
        self.admin = User.objects.get(username=seed.ADMIN_USER)
        self.idiq = Contract.objects.get(parent=None)

    def test_creates_superuser(self):
        assert self.admin.is_superuser
        assert self.admin.is_staff

    def test_superuser_password(self):
        assert self.admin.check_password(seed.ADMIN_PASSWORD)

    def test_superuser_email(self):
        assert self.admin.email == seed.ADMIN_EMAIL

    def test_creates_prime(self):
        assert Company.objects.get(name=seed.PRIME_NAME)

    def test_creates_subcontractors(self):
        # Should be prime + contractors
        assert Company.objects.count() == seed.NUM_SUBCONTRACTORS + 1

    def test_creates_employees_for_prime(self):
        prime = Company.objects.get(name=seed.PRIME_NAME)
        assert len(prime.employees) == NUM_PEOPLE_PRIME

    def test_creates_employees_for_subcontractors(self):
        sub = Company.objects.exclude(name=seed.PRIME_NAME).first()
        assert len(sub.employees) == NUM_PEOPLE_SUB

    def test_creates_5_year_idiq(self):
        assert _duration_in_years(self.idiq) == 5

    def test_creates_idiq_task_orders(self):
        assert Contract.objects.filter(parent=self.idiq).count() == NUM_TASK_ORDERS

    def test_task_order_duration_is_1_year(self):
        task_order = Contract.objects.filter(parent=self.idiq).first()

        assert _duration_in_years(task_order) == 1

    def test_populates_proposal_project(self):
        proposal = Project.objects.filter(contract=None).first()

        assignment_count = Assignment.objects.filter(project=proposal).count()
        assert assignment_count == seed.NUM_PROPOSAL_WORKERS


class TestPopulateTaskOrders(TestCase):

    def test_requires_3_task_orders(self):
        workers = WorkerFactory.create_batch(3)
        task_orders = ContractFactory.create_batch(2)

        with pytest.raises(Exception) as excinfo:
            seed.populate_task_orders(workers=workers, task_orders=task_orders)

        assert 'at least 3 task orders required' in str(excinfo)

    def test_assigns_projects(self):
        with patch.object(seed, 'generate_appointments') as mock_generate_appointments:
            workers = WorkerFactory.create_batch(3)
            task_orders = ContractFactory.create_batch(5)

            seed.populate_task_orders(workers=workers, task_orders=task_orders)

        assigned_worker_count = Worker.objects.annotate(num_assignment=Count('assignments'))\
            .filter(num_assignment__gt=0, num_assignment__lte=3).count()

        assert assigned_worker_count == len(workers)
        mock_generate_appointments.assert_called()


def _duration_in_years(contract):
    """
    Test helper: Computes duration in years of during attribute for a contract

    Args:
        during (DateRange): duration

    Returns:
        (int) duration in years
    """
    return relativedelta(contract.during.upper, contract.during.lower).years
