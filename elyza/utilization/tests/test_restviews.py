import json
from test_plus.test import APITestCase
from django.contrib.auth.models import Permission
from ..models import (Assignment, Project)
from .factories import (ProjectFactory, AssignmentFactory, WorkerFactory, AppointmentFactory)


class BaseTestCase(APITestCase):

    def setUp(self):
        self.user = self.make_user(perms=['utilization.*'])


class TestProjectListAPIView(BaseTestCase):

    url_name = 'utilization:api:project-list'

    def test_login_required(self):
        """
        Verify that a user must be logged in to access the API
        """
        response = self.get(self.url_name)
        self.response_403()

        expected_response = {"detail": "Authentication credentials were not provided."}
        self.assertJSONEqual(response.content, expected_response)

        with self.login(self.user):
            self.get_check_200(self.url_name)

    def test_permission_required(self):
        """
        Verify that a user must have utilization.view_project permission
        """
        # Remove the user's permission
        required_permission = Permission.objects.get(codename='view_project')
        self.user.user_permissions.remove(required_permission)

        with self.login(self.user):
            response = self.get(self.url_name)
            self.response_403()

            expected_response = {"detail": "You do not have permission to perform this action."}
            self.assertJSONEqual(response.content, expected_response)

    def test_allowed_verbs(self):
        """
        Verify HTTP verbs supported
        """
        with self.login(self.user):
            response = self.options(self.url_name)

            expected = ['GET', 'HEAD', 'OPTIONS', ]
            actual = response['Allow'].split(', ')

            self.assertCountEqual(expected, actual)

    def test_response(self):
        """
        Verify response format
        """
        project = ProjectFactory()

        expected = [
            {
                'title': project.title,
                'uuid': project.uuid,
                'budget_in_dollars': project.budget_in_dollars,
            },

        ]

        with self.login(self.user):
            response = self.get(self.url_name)
            actual = json.loads(response.content)

            self.assertCountEqual(expected, actual)


class TestAssignmentListAPIView(BaseTestCase):

    url_name = 'utilization:api:assignment-list'

    def test_login_required(self):
        """
        Verify that a user must be logged in to access the API
        """
        response = self.get(self.url_name)
        self.response_403()

        expected_response = {"detail": "Authentication credentials were not provided."}
        self.assertJSONEqual(response.content, expected_response)

        with self.login(self.user):
            self.get_check_200(self.url_name)

    def test_allowed_verbs(self):
        """
        Verify HTTP verbs supported
        """
        with self.login(self.user):
            response = self.options(self.url_name)

            expected = ['GET', 'POST', 'HEAD', 'OPTIONS', ]
            actual = response['Allow'].split(', ')

            self.assertCountEqual(expected, actual)

    def test_view_permission(self):
        """
        Verify that a user must have utilization.view_project permission
        """
        # Remove the user's permission
        required_permission = Permission.objects.get(codename='view_assignment')
        self.user.user_permissions.remove(required_permission)

        with self.login(self.user):
            response = self.get(self.url_name)
            self.response_403()

            expected_response = {"detail": "You do not have permission to perform this action."}
            self.assertJSONEqual(response.content, expected_response)

    def test_read(self):
        """
        Verify response format for GET
        """
        AssignmentFactory.create_batch(2)

        with self.login(self.user):
            response = self.get(self.url_name)
            actual = json.loads(response.content)

            assert len(actual) == 2

    def test_read_filter(self):
        """
        Verify filtering using querystring
        """
        assignments = AssignmentFactory.create_batch(5)

        with self.login(self.user):
            response = self.get(self.url_name, data={'worker': assignments[0].worker.uuid})
            actual = json.loads(response.content)

            assert len(actual) == 1
            self.assertEqual(actual[0]['worker'], assignments[0].worker.uuid)

    # pylint: disable=no-member
    def test_create(self):
        """
        Verify able to create assignments
        """
        project = ProjectFactory()
        worker = WorkerFactory()

        self.assertEqual(Assignment.objects.count(), 0)

        with self.login(self.user):
            self.post(self.url_name, data={'worker': worker.uuid, 'project': project.uuid})

            self.response_201()
            self.assertEqual(Assignment.objects.count(), 1)


class TestAssignmentAPIView(BaseTestCase):

    url_name = 'utilization:api:assignment'

    def setUp(self):
        self.assignment = AssignmentFactory()
        super(TestAssignmentAPIView, self).setUp()

    def test_login_required(self):
        """
        Verify that a user must be logged in to access the API
        """
        response = self.get(self.url_name, uuid=self.assignment.uuid)

        self.response_403()

        expected_response = {"detail": "Authentication credentials were not provided."}
        self.assertJSONEqual(response.content, expected_response)

        with self.login(self.user):
            self.get_check_200(self.url_name, uuid=self.assignment.uuid)

    def test_allowed_verbs(self):
        """
        Verify HTTP verbs supported
        """
        with self.login(self.user):
            response = self.options(self.url_name, uuid=self.assignment.uuid)

            expected = ['GET', 'PUT', 'PATCH', 'HEAD', 'OPTIONS', 'DELETE']
            actual = response['Allow'].split(', ')

            self.assertCountEqual(expected, actual)

    def test_view_permission(self):
        """
        Verify that a user must have permission
        """
        # Remove the user's permission
        required_permission = Permission.objects.get(codename='view_assignment')
        self.user.user_permissions.remove(required_permission)

        with self.login(self.user):
            response = self.get(self.url_name, uuid=self.assignment.uuid)
            self.response_403()

            expected_response = {"detail": "You do not have permission to perform this action."}
            self.assertJSONEqual(response.content, expected_response)

    def test_update(self):
        worker = WorkerFactory()

        with self.login(self.user):
            self.patch(self.url_name, uuid=self.assignment.uuid, data={'worker': worker.uuid})

            self.response_200()

            self.assignment.refresh_from_db()
            self.assertEqual(self.assignment.worker, worker)

    def test_delete(self):
        with self.login(self.user):
            self.delete(self.url_name, uuid=self.assignment.uuid)

            # pylint: disable=no-member
            self.assertEqual(Assignment.objects.count(), 0)


class TestProjectAPIView(BaseTestCase):

    url_name = 'utilization:api:project'

    def setUp(self):
        self.project = ProjectFactory()
        super(TestProjectAPIView, self).setUp()

    def test_login_required(self):
        """
        Verify that a user must be logged in to access the API
        """
        response = self.get(self.url_name, uuid=self.project.uuid)

        self.response_403()

        expected_response = {"detail": "Authentication credentials were not provided."}
        self.assertJSONEqual(response.content, expected_response)

        with self.login(self.user):
            self.get_check_200(self.url_name, uuid=self.project.uuid)

    def test_allowed_verbs(self):
        """
        Verify HTTP verbs supported
        """
        with self.login(self.user):
            response = self.options(self.url_name, uuid=self.project.uuid)

            expected = ['GET', 'HEAD', 'PUT', 'PATCH', 'OPTIONS']
            actual = response['Allow'].split(', ')

            self.assertCountEqual(expected, actual)

    def test_view_permission(self):
        """
        Verify that a user must have permission
        """
        # Remove the user's permission
        required_permission = Permission.objects.get(codename='view_project')
        self.user.user_permissions.remove(required_permission)

        with self.login(self.user):
            response = self.get(self.url_name, uuid=self.project.uuid)
            self.response_403()

            expected_response = {"detail": "You do not have permission to perform this action."}
            self.assertJSONEqual(response.content, expected_response)

    def test_update(self):
        with self.login(self.user):
            self.patch(self.url_name, uuid=self.project.uuid, data={'title': 'sometitle99'})

            self.response_200()

            self.project.refresh_from_db()
            self.assertEqual(self.project.title, 'sometitle99')


class TestWorkerAPIView(BaseTestCase):

    url_name = 'utilization:api:worker'

    def setUp(self):
        self.worker = WorkerFactory()
        super().setUp()

    def test_login_required(self):
        """
        Verify that a user must be logged in to access the API
        """
        response = self.get(self.url_name, uuid=self.worker.uuid)

        self.response_403()

        expected_response = {"detail": "Authentication credentials were not provided."}
        self.assertJSONEqual(response.content, expected_response)

        with self.login(self.user):
            self.get_check_200(self.url_name, uuid=self.worker.uuid)

    def test_allowed_verbs(self):
        """
        Verify HTTP verbs supported
        """
        with self.login(self.user):
            response = self.options(self.url_name, uuid=self.worker.uuid)

            expected = ['GET', 'HEAD', 'PUT', 'PATCH', 'OPTIONS']
            actual = response['Allow'].split(', ')

            self.assertCountEqual(expected, actual)

    def test_view_permission(self):
        """
        Verify that a user must have permission
        """
        # Remove the user's permission
        required_permission = Permission.objects.get(codename='view_worker')
        self.user.user_permissions.remove(required_permission)

        with self.login(self.user):
            response = self.get(self.url_name, uuid=self.worker.uuid)
            self.response_403()

            expected_response = {"detail": "You do not have permission to perform this action."}
            self.assertJSONEqual(response.content, expected_response)

    def test_update(self):
        with self.login(self.user):
            self.patch(self.url_name, uuid=self.worker.uuid, data={'first_name': 'billy'})

            self.response_200()

            self.worker.refresh_from_db()
            self.assertEqual(self.worker.first_name, 'billy')


class TestAppointmentAPIView(BaseTestCase):

    url_name = 'utilization:api:appointment'

    def setUp(self):
        self.appointment = AppointmentFactory()
        super().setUp()

    def test_login_required(self):
        """
        Verify that a user must be logged in to access the API
        """
        response = self.get(self.url_name, uuid=self.appointment.uuid)

        self.response_403()

        expected_response = {"detail": "Authentication credentials were not provided."}
        self.assertJSONEqual(response.content, expected_response)

        with self.login(self.user):
            self.get_check_200(self.url_name, uuid=self.appointment.uuid)

    def test_allowed_verbs(self):
        """
        Verify HTTP verbs supported
        """
        with self.login(self.user):
            response = self.options(self.url_name, uuid=self.appointment.uuid)

            expected = ['GET', 'HEAD', 'PUT', 'PATCH', 'OPTIONS']
            actual = response['Allow'].split(', ')

            self.assertCountEqual(expected, actual)

    def test_view_permission(self):
        """
        Verify that a user must have permission
        """
        # Remove the user's permission
        required_permission = Permission.objects.get(codename='view_appointment')
        self.user.user_permissions.remove(required_permission)

        with self.login(self.user):
            response = self.get(self.url_name, uuid=self.appointment.uuid)
            self.response_403()

            expected_response = {"detail": "You do not have permission to perform this action."}
            self.assertJSONEqual(response.content, expected_response)

    def test_update(self):
        with self.login(self.user):
            self.patch(self.url_name, uuid=self.appointment.uuid, data={'hours': 8})

            self.response_200()

            self.appointment.refresh_from_db()
            self.assertEqual(self.appointment.hours, 8)

