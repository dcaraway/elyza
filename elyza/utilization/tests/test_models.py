from test_plus.test import TestCase
from .factories import (WorkerFactory, ProjectFactory, CompanyFactory, ActualFactory,
                        AppointmentFactory, AssignmentFactory,)
from elyza.utilization.models import (Worker, Project, Actual, Appointment, Assignment,)
from dateutil.relativedelta import relativedelta
from datetime import date
import pytest
from django.db import transaction
from django.db.models.deletion import ProtectedError
from django.core.exceptions import ValidationError

TODAY = date.today()
YESTERDAY = TODAY - relativedelta(days=1)
TOMORROW = TODAY + relativedelta(days=1)
TWO_DAYS_FROM_NOW = TODAY + relativedelta(days=2)


class TestWorker(TestCase):

    def test_company_delete_cascades(self):
        company = CompanyFactory()
        WorkerFactory.create_batch(5, company=company)

        self.assertEqual(5, Worker.objects.count())
        company.delete()

        self.assertEqual(0, Worker.objects.count())


class TestProject(TestCase):

    def test_construction(self):
        project = ProjectFactory()
        self.assertTrue(project)

    def test_default_values(self):
        project = Project()

        self.assertEqual(project.budget_in_dollars, 0)


class TestActual(TestCase):

    def test_overlap_same_assignment1(self):
        # Test database-level constraint
        actual1 = ActualFactory(during=(TODAY, TWO_DAYS_FROM_NOW))

        with pytest.raises(Exception) as excinfo:
            # required to prevent transaction management error, see
            # https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                ActualFactory(during=(YESTERDAY, TOMORROW),
                              worker=actual1.worker,
                              project=actual1.project,
                              task_id=actual1.task_id,
                              lcat=actual1.lcat)
            import pdb
            pdb.set_trace()


        assert 'exclusion constraint' in str(excinfo)

    def test_overlap_diff_assignment(self):
        actual1 = ActualFactory(during=(TODAY, TWO_DAYS_FROM_NOW))
        actual2 = ActualFactory(during=(YESTERDAY, TOMORROW))

        assert actual1.worker != actual2.worker
        assert actual1.project != actual2.project
        assert Actual.objects.count() == 2

    def test_overlap_same_assignment2(self):
        # Test database-level constraint
        actual1 = ActualFactory(during=(YESTERDAY, TOMORROW))

        with pytest.raises(Exception) as excinfo:
            # required to prevent transaction management error, see
            # https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                ActualFactory(during=(TODAY, TWO_DAYS_FROM_NOW),
                              worker=actual1.worker,
                              project=actual1.project,
                              task_id=actual1.task_id,
                              lcat=actual1.lcat)

        assert 'exclusion constraint' in str(excinfo)

    def test_overlap_ending(self):
        actual1 = ActualFactory(during=(YESTERDAY, TODAY))
        actual2 = ActualFactory(during=(TODAY, TOMORROW))
        actual2.clean()

        assert actual1.worker != actual2.worker
        assert actual1.project != actual2.project
        assert Actual.objects.count() == 2

    def test_actual_update(self):
        actual1 = ActualFactory(during=(YESTERDAY, TODAY))
        actual1.clean()

        actual1.during = (YESTERDAY, TOMORROW)
        actual1.save()
        actual1.clean()

        assert Actual.objects.count() == 1

    def test_clean_prevents_overlap(self):
        actual1 = ActualFactory(during=(YESTERDAY, TOMORROW))
        actual2 = ActualFactory.build(during=(TODAY, TWO_DAYS_FROM_NOW),
                                      worker=actual1.worker,
                                      project=actual1.project,
                                      task_id=actual1.task_id,
                                      lcat=actual1.lcat)

        with pytest.raises(ValidationError) as ex:
            actual2.clean()

        assert 'overlap' in str(ex.value)


class TestAppointment(TestCase):

    def test_overlap_same_assignment1(self):
        # Test database level validation
        appointment1 = AppointmentFactory(during=(TODAY, TWO_DAYS_FROM_NOW))

        with pytest.raises(Exception) as excinfo:
            # required to prevent transaction management error, see
            # https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                AppointmentFactory(during=(YESTERDAY, TOMORROW), worker=appointment1.worker, project=appointment1.project)

        assert 'exclusion constraint' in str(excinfo)

    def test_overlap_diff_assignment1(self):
        appointment1 = AppointmentFactory(during=(TODAY, TWO_DAYS_FROM_NOW))
        appointment2 = AppointmentFactory(during=(YESTERDAY, TOMORROW))

        assert appointment1.worker != appointment2.worker
        assert appointment1.project != appointment2.project
        assert Appointment.objects.count() == 2

    def test_overlap_same_assignment2(self):
        # Test database level validation
        appointment1 = AppointmentFactory(during=(YESTERDAY, TOMORROW))

        with pytest.raises(Exception) as excinfo:
            # required to prevent transaction management error, see
            # https://stackoverflow.com/questions/21458387/transactionmanagementerror-you-cant-execute-queries-until-the-end-of-the-atom
            with transaction.atomic():
                AppointmentFactory(during=(TODAY, TWO_DAYS_FROM_NOW), worker=appointment1.worker, project=appointment1.project)

        assert 'exclusion constraint' in str(excinfo)

    def test_overlap_ending(self):
        appointment1 = AppointmentFactory(during=(YESTERDAY, TODAY))
        appointment2 = AppointmentFactory(during=(TODAY, TOMORROW))
        appointment2.clean()

        assert appointment1.worker != appointment2.worker
        assert appointment1.project != appointment2.project
        assert Appointment.objects.count() == 2

    def test_appointment_update(self):
        appointment1 = AppointmentFactory(during=(YESTERDAY, TODAY))
        appointment1.clean()

        appointment1.during = (YESTERDAY, TOMORROW)
        appointment1.save()
        appointment1.clean()

        assert Appointment.objects.count() == 1

    def test_clean_prevents_overlap(self):
        appointment1 = AppointmentFactory(during=(YESTERDAY, TOMORROW))
        appointment2 = AppointmentFactory.build(during=(TODAY, TWO_DAYS_FROM_NOW),
                                                worker=appointment1.worker,
                                                project=appointment1.project)

        with pytest.raises(ValidationError) as ex:
            appointment2.clean()

        assert 'overlap' in str(ex.value)

