from datetime import date
import pytest
from test_plus.test import TestCase
from elyza.utilization.tests.factories import (DateRange,
                                               AssignmentFactory,
                                               ContractFactory,
                                               AppointmentFactory,
                                               WorkerFactory,)
from elyza.utilization import services
from elyza.utilization.services import (MaxHoursEntry,
                                        DurationSegment,
                                        monthly_workhours)
from elyza.utilization.models import Appointment


# pylint: disable=no-self-use
class TestGenerateAppointments(TestCase):

    def test_must_be_same_worker(self):
        assignments = AssignmentFactory.create_batch(2)

        with pytest.raises(Exception) as excinfo:
            services.generate_appointments(assignments=assignments)

        assert services.MIXED_WORKERS_ERROR_MESSAGE in str(excinfo)

    def test_must_not_have_appointments(self):
        assignment = AssignmentFactory()
        AppointmentFactory(worker=assignment.worker)

        with pytest.raises(Exception) as excinfo:
            services.generate_appointments(assignments=[assignment])

        assert services.ASSIGNMENT_HAS_APPOINTMENTS_ERROR_MESSAGE in str(excinfo)

    # pylint: disable=no-member
    def test_makes_appointments_for_assignments(self):
        worker = WorkerFactory()
        task_orders = ContractFactory.create_batch(2)

        assignments = [AssignmentFactory(worker=worker, project=to.project) for to in task_orders]

        services.generate_appointments(assignments=assignments)

        assert Appointment.objects.filter(project=assignments[0].project).count() >= 1
        assert Appointment.objects.filter(project=assignments[1].project).count() >= 1

    def test_single_month_single_task(self):
        # First of this month
        start_date = date(2018, 4, 1)
        # end date, not inclusive
        end_date = date(2018, 5, 1)

        # one month task order
        task_order = ContractFactory(during=DateRange(lower=start_date, upper=end_date))
        assignment = AssignmentFactory(project=task_order.project)

        appointments = services.generate_appointments(assignments=[assignment])

        assert len(appointments) == 1
        assert appointments[0].project.contract == task_order
        assert appointments[0].hours == 168
        assert appointments[0].during == DateRange(lower=start_date, upper=end_date)

    def test_multi_month_single_task(self):
        # First of this month
        start_date = date(2018, 4, 1)
        # end date, not inclusive
        end_date = date(2018, 6, 1)

        # two month task order
        task_order = ContractFactory(during=DateRange(lower=start_date, upper=end_date))
        assignment = AssignmentFactory(project=task_order.project)

        appointments = services.generate_appointments(assignments=[assignment])

        assert len(appointments) == 2
        assert appointments[0].hours == 168
        assert appointments[1].hours == 184

    def test_single_month_multi_task(self):
        start_date1 = date(2018, 4, 1)
        start_date2 = start_date1

        # end date, not inclusive
        end_date = date(2018, 5, 1)

        # one month task order
        task_order1 = ContractFactory(during=DateRange(lower=start_date1, upper=end_date))
        task_order2 = ContractFactory(during=DateRange(lower=start_date2, upper=end_date))

        worker = WorkerFactory()

        assignment1 = AssignmentFactory(project=task_order1.project, worker=worker)
        assignment2 = AssignmentFactory(project=task_order2.project, worker=worker)

        appointments = list(services.generate_appointments(assignments=[assignment1, assignment2]))

        assert len(appointments) == 2
        assert appointments[0].hours == 84.0
        assert appointments[1].hours == 84.0

    def test_single_task_mixed_month_multi_task(self):
        # in this example, one of the tasks only covers part of the month
        start_date1 = date(2018, 4, 1)
        start_date2 = date(2018, 4, 15)

        # end date, not inclusive
        end_date = date(2018, 5, 1)

        # one month task order
        task_order1 = ContractFactory(during=DateRange(lower=start_date1, upper=end_date))
        task_order2 = ContractFactory(during=DateRange(lower=start_date2, upper=end_date))

        worker = WorkerFactory()

        assignment1 = AssignmentFactory(project=task_order1.project, worker=worker)
        assignment2 = AssignmentFactory(project=task_order2.project, worker=worker)

        appointments = list(services.generate_appointments(assignments=[assignment1, assignment2]))

        assert len(appointments) == 2
        assert appointments[0].hours == 80.0
        assert appointments[1].hours == 88.0


class TestMonthlyWorkHours(TestCase):

    def test_single_interval(self):
        start = date(2018, 4, 1)
        end = date(2018, 5, 1)
        task_order = ContractFactory(during=DateRange(start, end))

        expected = [
            MaxHoursEntry(hours=168,
                          during=DurationSegment(start=start, end=end),
                          contract_id=task_order.id),
        ]
        actual = list(monthly_workhours(task_order))
        assert expected == actual

    def test_multiple_intervals(self):
        start = date(2018, 4, 1)
        end = date(2018, 6, 15)
        task_order = ContractFactory(during=DateRange(start, end))

        expected = [
            MaxHoursEntry(hours=168,
                          during=DurationSegment(start=start,
                                                 end=date(2018, 5, 1)),
                          contract_id=task_order.id),
            MaxHoursEntry(hours=184,
                          during=DurationSegment(start=date(2018, 5, 1),
                                                 end=date(2018, 6, 1)),
                          contract_id=task_order.id),
            MaxHoursEntry(hours=80,
                          during=DurationSegment(start=date(2018, 6, 1),
                                                 end=date(2018, 6, 15)),
                          contract_id=task_order.id),
        ]
        actual = list(monthly_workhours(task_order))
        assert expected == actual


class TestToMonthlyIntervals(TestCase):

    def test_first_of_month_start(self):
        start = date(2018, 4, 1)
        end = date(2018, 5, 1)

        expected = [DurationSegment(start=start, end=end)]
        actual = list(services.to_monthly_intervals(start=start, end=end))

        assert expected == actual

    def test_mid_month_start(self):
        start = date(2018, 4, 15)
        end = date(2018, 5, 1)

        expected = [DurationSegment(start=start, end=end)]
        actual = list(services.to_monthly_intervals(start=start, end=end))

        assert expected == actual

    def test_end_month_start(self):
        start = date(2018, 4, 30)
        end = date(2018, 5, 1)

        expected = [DurationSegment(start=start, end=end)]
        actual = list(services.to_monthly_intervals(start=start, end=end))

        assert expected == actual

    def test_mid_month_end(self):
        start = date(2018, 4, 1)
        end = date(2018, 5, 15)

        expected = [
            DurationSegment(start=start, end=date(2018, 5, 1)),
            DurationSegment(start=date(2018, 5, 1), end=end),
        ]
        actual = list(services.to_monthly_intervals(start=start, end=end))

        assert expected == actual

    def test_end_month_end(self):
        start = date(2018, 4, 1)
        end = date(2018, 5, 31)

        expected = [
            DurationSegment(start=start, end=date(2018, 5, 1)),
            DurationSegment(start=date(2018, 5, 1), end=end),
        ]
        actual = list(services.to_monthly_intervals(start=start, end=end))

        assert expected == actual

    def test_mid_month_start_and_end(self):
        start = date(2018, 4, 15)
        end = date(2018, 5, 15)

        expected = [
            DurationSegment(start=start, end=date(2018, 5, 1)),
            DurationSegment(start=date(2018, 5, 1), end=end),
        ]
        actual = list(services.to_monthly_intervals(start=start, end=end))

        assert expected == actual
