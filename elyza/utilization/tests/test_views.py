from django.test import RequestFactory
from test_plus.test import TestCase

from .factories import WorkerFactory, ProjectFactory


class BaseTestCase(TestCase):

    def setUp(self):
        self.user = self.make_user(perms=['utilization.*'])
        self.factory = RequestFactory()


class TestWorkerListView(BaseTestCase):

    def test_restrictions(self):
        bad_user = self.make_user('baduser')

        # Ensure user has required permission
        good_user = self.make_user('gooduser', perms=['utilization.view_worker'])

        with self.login(bad_user):
            self.get('utilization:worker-list')
            self.response_403()

        with self.login(good_user):
            self.get_check_200('utilization:worker-list')


class TestWorkerDetailView(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.worker = WorkerFactory()

    def test_restrictions(self):
        bad_user = self.make_user('baduser')

        # Ensure user has required permission
        good_user = self.make_user('gooduser', perms=['utilization.view_worker'])

        with self.login(bad_user):
            self.get('utilization:worker-detail', uuid=self.worker.uuid)
            self.response_403()

        with self.login(good_user):
            self.get_check_200('utilization:worker-detail', uuid=self.worker.uuid)

    def test_access_own_worker(self):
        myuser = self.make_user('myuser')

        with self.login(myuser):
            self.get('utilization:worker-detail', uuid=self.worker.uuid)
            self.response_403()

            # Now set the worker's user to be myuser
            self.worker.user = myuser
            self.worker.save()

            self.get('utilization:worker-detail', uuid=self.worker.uuid)
            self.response_200()


class TestProjectListView(BaseTestCase):

    def test_restrictions(self):
        bad_user = self.make_user('baduser')

        # Ensure user has required permission
        good_user = self.make_user('gooduser', perms=['utilization.view_project'])

        with self.login(bad_user):
            self.get('utilization:project-list')
            self.response_403()

        with self.login(good_user):
            self.get_check_200('utilization:project-list')


class TestProjectCreateView(BaseTestCase):

    def test_restrictions(self):
        bad_user = self.make_user('baduser')

        # Ensure user has required permission
        good_user = self.make_user('gooduser', perms=['utilization.add_project'])

        with self.login(bad_user):
            self.get('utilization:project-create')
            self.response_403()

        with self.login(good_user):
            self.get_check_200('utilization:project-create')


class TestProjectDetailView(BaseTestCase):

    def setUp(self):
        super().setUp()
        self.project = ProjectFactory()

    def test_restrictions(self):
        bad_user = self.make_user('baduser')

        # Ensure user has required permission
        good_user = self.make_user('gooduser', perms=['utilization.view_project'])

        with self.login(bad_user):
            self.get('utilization:project-detail', uuid=self.project.uuid)
            self.response_403()

        with self.login(good_user):
            self.get_check_200('utilization:project-detail', uuid=self.project.uuid)
