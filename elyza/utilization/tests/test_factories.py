from unittest.mock import (Mock, patch)
from datetime import date
from dateutil.relativedelta import relativedelta
from freezegun import freeze_time
from elyza.utilization.tests.factories import determine_contract_during


class TestDetermineContractDuring:
    """
    Tests for determine_contract_during() helper method
    """

    @freeze_time("2012-01-14")
    def test_contract_has_no_parent(self):
        mock_contract = Mock()
        mock_contract.parent = None

        result = determine_contract_during(mock_contract)

        expected_start = date.today() - relativedelta(months=5, day=1)
        expected_end = expected_start + relativedelta(days=365)

        expected = (expected_start, expected_end)

        assert result == expected

    @freeze_time("2012-01-14")
    def test_when_short_contract(self):
        mock_contract = Mock()
        mock_contract.parent.during = (
            date.today(),
            date.today() + relativedelta(months=11)
        )

        result = determine_contract_during(mock_contract)

        assert result == mock_contract.parent.during

    @freeze_time("2012-01-14")
    def test_when_long_contract(self):
        mock_contract = Mock()
        mock_contract.parent.during = (
            date.today(),
            date.today() + relativedelta(months=36)
        )

        result = determine_contract_during(mock_contract)
        assert result[0] >= mock_contract.parent.during[0]
        assert result[1] <= mock_contract.parent.during[1]
