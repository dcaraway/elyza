from datetime import date
import random
from collections import namedtuple
import factory
import factory.fuzzy
from dateutil.relativedelta import relativedelta
from elyza.users.tests.factories import UserFactory
from elyza.data import RESUMES

DAYS_PER_YEAR = 365
DateRange = namedtuple('DateRange', field_names=['lower', 'upper'])


# pylint: disable=too-few-public-methods,unnecessary-lambda,no-member,unused-argument
class CompanyFactory(factory.django.DjangoModelFactory):
    name = factory.Faker('company')

    class Meta:
        model = 'utilization.Company'


class WorkerFactory(factory.django.DjangoModelFactory):
    user = factory.SubFactory(UserFactory)
    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')
    accounting_id = factory.Sequence(lambda n: 'employeeid-{0}'.format(n))
    company = factory.SubFactory(CompanyFactory)
    resume = factory.LazyAttribute(lambda worker: random.choice(RESUMES))

    class Meta:
        model = 'utilization.Worker'

    @factory.post_generation
    def labor_categories(self, create, extracted, **kwargs):
        if not create:
            # Simple build, do nothing.
            return

        if extracted:
            # A list of labor categories were passed in, use them
            for lcat in extracted:
                self.labor_categories.add(lcat)


class LaborCategoryFactory(factory.django.DjangoModelFactory):
    code = factory.Sequence(lambda n: 'code-{0}'.format(n))
    title = factory.Faker('job')
    description = factory.Sequence(lambda n: 'description-{0}'.format(n))

    class Meta:
        model = 'utilization.LaborCategory'


class ProjectFactory(factory.django.DjangoModelFactory):
    title = factory.Sequence(lambda n: 'title-{0}'.format(n))
    description = factory.Sequence(lambda n: 'description-{0}'.format(n))

    class Meta:
        model = 'utilization.Project'


def determine_contract_during(contract):
    """
    Helper method for contract construction.
    Checks if there's a parent and if so, make
    sure the duration of the child contract is within the
    duration of the parent contract
    """
    try:
        parent_duration = contract.parent.during[1] - contract.parent.during[0]
    except AttributeError:
        start = date.today() - relativedelta(months=5, day=1)
        end = start + relativedelta(days=DAYS_PER_YEAR)
        return DateRange(lower=start, upper=end)

    if parent_duration.days <= DAYS_PER_YEAR:
        return contract.parent.during

    # Start a random number of days from beginning of parent
    days_in = random.randint(0, parent_duration.days - DAYS_PER_YEAR)

    start = contract.parent.during[0]+relativedelta(days=days_in)
    end = start + relativedelta(days=DAYS_PER_YEAR+1)

    return DateRange(lower=start, upper=end)


class ContractFactory(factory.django.DjangoModelFactory):
    project = factory.SubFactory(ProjectFactory)
    accounting_id = factory.Sequence(lambda n: 'nav-id-{0}'.format(n))

    during = factory.LazyAttribute(determine_contract_during)

    class Meta:
        model = 'utilization.Contract'


class AssignmentFactory(factory.django.DjangoModelFactory):
    project = factory.SubFactory(ProjectFactory)
    worker = factory.SubFactory(WorkerFactory)

    class Meta:
        model = 'utilization.Assignment'


class AppointmentFactory(factory.django.DjangoModelFactory):
    hours = 12
    during = (date.today() - relativedelta(days=1), date.today() + relativedelta(days=9))

    project = factory.SubFactory(ProjectFactory)
    worker = factory.SubFactory(WorkerFactory)

    class Meta:
        model = 'utilization.Appointment'


class ActualFactory(factory.django.DjangoModelFactory):
    hours = 7
    during = (date.today() - relativedelta(days=1), date.today() + relativedelta(days=9))
    task_id = factory.Sequence(lambda n: 'task-{0}'.format(n))

    lcat = factory.SubFactory(LaborCategoryFactory)
    project = factory.SubFactory(ProjectFactory)
    worker = factory.SubFactory(WorkerFactory)

    class Meta:
        model = 'utilization.Actual'
