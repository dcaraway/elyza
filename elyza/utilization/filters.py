from django_filters import rest_framework as filters
from .models import Assignment


class AssignmentFilter(filters.FilterSet):
    """
    A query filter for the Assignment model
    """
    worker = filters.CharFilter(name='worker__uuid')

    # pylint: disable=too-few-public-methods
    class Meta:
        model = Assignment
        fields = ('worker',)
