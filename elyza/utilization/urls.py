from django.conf.urls import (url, include)
from . import views
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required

urlpatterns = [
    url(
        regex=r'^profiles/$',
        view=views.WorkerListView.as_view(),
        name='worker-list'
    ),
    url(
        regex=r'^profiles/(?P<uuid>[\w]+)/$',
        view=views.WorkerDetailView.as_view(),
        name='worker-detail'
    ),
    url(
        regex=r'^projects/$',
        view=views.ProjectListView.as_view(),
        name='project-list'
    ),
    url(
        regex=r'^projects/new$',
        view=views.ProjectCreateView.as_view(),
        name='project-create'
    ),
    url(
        regex=r'^projects/(?P<uuid>[\w]+)/$',
        view=views.ProjectDetailView.as_view(),
        name='project-detail'
    ),

    url(regex=r'^frontend/', view=login_required(TemplateView.as_view(template_name='index.html'))),

    url(r'^api/', include('elyza.utilization.resturls', namespace='api')),
]
