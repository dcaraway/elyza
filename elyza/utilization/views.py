from django.views.generic import DetailView, ListView, CreateView
from django.urls import reverse_lazy
from braces.views import LoginRequiredMixin, PermissionRequiredMixin, UserPassesTestMixin
from django.db.models import Q
from .models import (Worker, Project,)
from .restviews import *


class SecureView(LoginRequiredMixin, PermissionRequiredMixin):
    raise_exception = True
    redirect_unauthenticated_users = True


class CustomSecureView(LoginRequiredMixin, UserPassesTestMixin):
    raise_exception = True
    redirect_unauthenticated_users = True


class WorkerListView(SecureView, ListView):
    model = Worker
    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'
    paginate_by = 10
    context_object_name = 'workers'

    permission_required = 'utilization.view_worker'

    def get_queryset(self):
        qs = super().get_queryset()

        query = self.request.GET.get('q')

        if query:
            return qs.filter(Q(last_name__icontains=query) | Q(first_name__icontains=query))

        return qs


class WorkerDetailView(CustomSecureView, DetailView):
    model = Worker

    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'

    context_object_name = 'worker'

    def test_func(self, user):
        return user.has_perm('utilization.view_worker')\
            or Worker.objects.filter(user=user, uuid=self.kwargs['uuid'])


class ProjectCreateView(SecureView, CreateView):
    model = Project
    fields = ['title', 'description', 'budget_in_dollars', ]

    success_url = reverse_lazy('utilization:project-list')
    permission_required = 'utilization.add_project'


class ProjectListView(SecureView, ListView):
    model = Project

    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'

    context_object_name = 'projects'
    permission_required = 'utilization.view_project'


class ProjectDetailView(SecureView, DetailView):
    model = Project

    slug_field = 'uuid'
    slug_url_kwarg = 'uuid'

    context_object_name = 'project'
    permission_required = 'utilization.view_project'
