from django.conf.urls import url
from .restviews import (ProjectListAPIView,
                        ProjectAPIView, AssignmentListAPIView,
                        AssignmentAPIView, WorkerAPIView, WorkerListAPIView,
                        AppointmentListAPIView, AppointmentAPIView)

urlpatterns = [
    url(
        regex=r'^projects/$',
        view=ProjectListAPIView.as_view(),
        name='project-list',
        ),

    url(
        regex=r'^projects/(?P<uuid>[\w]+)/$',
        view=ProjectAPIView.as_view(),
        name='project',
        ),

    url(
        regex=r'^assignments/$',
        view=AssignmentListAPIView.as_view(),
        name='assignment-list',
        ),

    url(
        regex=r'^assignments/(?P<uuid>[\w]+)/$',
        view=AssignmentAPIView.as_view(),
        name='assignment',
        ),

    url(
        regex=r'^workers/$',
        view=WorkerListAPIView.as_view(),
        name='worker-list',
        ),

    url(
        regex=r'^workers/(?P<uuid>[\w]+)/$',
        view=WorkerAPIView.as_view(),
        name='worker',
        ),

    url(
        regex=r'^appointments/$',
        view=AppointmentListAPIView.as_view(),
        name='appointment-list',
        ),

    url(
        regex=r'^appointments/(?P<uuid>[\w]+)/$',
        view=AppointmentAPIView.as_view(),
        name='appointment',
        ),
    ]
