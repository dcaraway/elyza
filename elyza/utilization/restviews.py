# TODO move this to views.py once we remove regular HTML views
# and move to react
from rest_framework import generics
from .models import (Project, Assignment, Worker, Appointment)
from .serializers import (ProjectSerializer, ProjectPartialSerializer,
                          AssignmentSerializer, WorkerSerializer, AppointmentSerializer)
from .filters import AssignmentFilter


class AssignmentListAPIView(generics.ListCreateAPIView):
    """
    List and create assignments
    """
    # pylint: disable=no-member
    serializer_class = AssignmentSerializer
    queryset = Assignment.objects.all()
    filter_class = AssignmentFilter


class AssignmentAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Retrieve, update and destroy assignments. Note when deleting that assignments
    must have actuals or appointments deleted first or the action will fail.
    """
    # pylint: disable=no-member
    serializer_class = AssignmentSerializer
    lookup_field = 'uuid'
    queryset = Assignment.objects.all()


class ProjectListAPIView(generics.ListAPIView):
    """
    List projects
    """
    # TODO also allow project creation at some point

    # pylint: disable=no-member
    queryset = Project.objects.all()
    serializer_class = ProjectPartialSerializer


class ProjectAPIView(generics.RetrieveUpdateAPIView):
    """
    Retrieve and update a project
    """
    # pylint: disable=no-member
    lookup_field = 'uuid'
    serializer_class = ProjectSerializer

    def get_queryset(self):
        queryset = Project.objects.all()
        queryset = self.serializer_class.setup_eager_loading(queryset)
        return queryset


class WorkerListAPIView(generics.ListAPIView):
    """
    Lists workers
    """
    # pylint: disable=no-member
    queryset = Worker.objects.all()
    serializer_class = WorkerSerializer


class WorkerAPIView(generics.RetrieveUpdateAPIView):
    """
    Retrieve and update a worker
    """
    # pylint: disable=no-member
    queryset = Worker.objects.all()
    lookup_field = 'uuid'
    serializer_class = WorkerSerializer


class AppointmentListAPIView(generics.ListAPIView):
    """
    Lists appointments
    """
    # pylint: disable=no-member
    queryset = Appointment.objects.all()
    serializer_class = AppointmentSerializer


class AppointmentAPIView(generics.RetrieveUpdateAPIView):
    """
    Retrieve and update a worker
    """
    # pylint: disable=no-member
    queryset = Appointment.objects.all()
    lookup_field = 'uuid'
    serializer_class = AppointmentSerializer
