from django_extensions.db.models import TimeStampedModel, TitleSlugDescriptionModel
from shortuuidfield import ShortUUIDField
from django.db import models
from django.contrib.postgres.fields import DateRangeField
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from elyza.users.models import User


# pylint: disable=no-member
class Worker(TimeStampedModel):
    user = models.OneToOneField(User, models.SET_NULL, unique=True, blank=True, null=True,)
    projects = models.ManyToManyField('Project', through='Assignment')

    # TODO remove these fields once we can guarantee that there will be a User
    first_name = models.CharField(max_length=30, blank=False)
    last_name = models.CharField(max_length=30, blank=False)
    # END OF TODO

    uuid = ShortUUIDField()
    company = models.ForeignKey('Company', on_delete=models.CASCADE, blank=False)
    resume = models.TextField()
    accounting_id = models.CharField(max_length=20, unique=True, blank=False, null=False)

    def get_contract_assignment(self, accounting_id):
        """Helper function to return the assignment for a given contract

        Returns:
            the Assignment
        """
        return self.assignment.get(project__contract__accounting_id__iexact=accounting_id)

    # pylint: disable=too-few-public-methods
    class Meta:
        db_table = 'utilization_worker'
        ordering = ["last_name"]
        permissions = (
            ("view_worker", "Can see workers and worker details"),
            )

    def __str__(self):
        return "{},{} ({})".format(self.last_name, self.first_name, self.accounting_id)


class Project(TimeStampedModel, TitleSlugDescriptionModel):
    """Data model for projects.
    """

    budget_in_dollars = models.PositiveIntegerField(default=0)
    uuid = ShortUUIDField()
    assignees = models.ManyToManyField('Worker', through='Assignment')

    # pylint: disable=too-few-public-methods,missing-docstring
    class Meta:
        db_table = 'utilization_project'
        permissions = (
            ("view_project", "Can see projects and project details"),
            )

    def __str__(self):
        return "{} ({})".format(self.title, self.uuid)


class LaborCategory(TimeStampedModel, TitleSlugDescriptionModel):
    """Data model for labor categories. Used to categorize workers for contract purposes.
    """
    code = models.CharField(max_length=20, unique=True)
    uuid = ShortUUIDField()
    at_customer_site = models.NullBooleanField()

    # pylint: disable=too-few-public-methods
    class Meta:
        db_table = 'utilization_labor_category'

    def __str__(self):
        return self.code


class Company(TimeStampedModel):
    name = models.CharField(max_length=60)
    uuid = ShortUUIDField()

    @property
    def employees(self):
        return Worker.objects.filter(company=self).order_by('last_name', 'first_name')

    # pylint: disable=too-few-public-methods
    class Meta:
        db_table = 'utilization_company'

    def __str__(self):
        return self.name


class Contract(TimeStampedModel):
    project = models.OneToOneField(Project, on_delete=models.CASCADE)
    uuid = ShortUUIDField()
    during = DateRangeField()
    accounting_id = models.CharField(max_length=20, unique=True, blank=False, null=False)
    parent = models.ForeignKey('Contract', on_delete=models.CASCADE, blank=True, null=True)
    task_order_id = models.CharField(max_length=20, unique=True, blank=False, null=True)
    classification = models.CharField(max_length=20, blank=False, null=True)
    award_dollars = models.PositiveIntegerField(default=0)

    # pylint: disable=too-few-public-methods
    class Meta:
        db_table = 'utilization_contract'

    def __str__(self):
        return "Accounting ID: {}".format(self.accounting_id)


class Assignment(TimeStampedModel):
    uuid = ShortUUIDField()
    project = models.ForeignKey('Project', related_name='assignments', on_delete=models.CASCADE)
    worker = models.ForeignKey('Worker', related_name='assignments', on_delete=models.CASCADE)

    unique_together = ('project', 'worker')

    # pylint: disable=too-few-public-methods
    class Meta:
        db_table = 'utilization_assignment'
        permissions = (
            ("view_assignment", "Can see assignments and assignment details"),
            )

    def __str__(self):
        return "Worker: {}, Project: {}".format(self.worker, self.project)


class Appointment(TimeStampedModel):
    uuid = ShortUUIDField()
    hours = models.DecimalField(max_digits=5, decimal_places=1, default=0)
    during = DateRangeField()
    project = models.ForeignKey('Project', related_name='appointments', on_delete=models.CASCADE)
    worker = models.ForeignKey('Worker', related_name='appointments', on_delete=models.CASCADE)

    class Meta:
        db_table = 'utilization_appointment'
        permissions = (
            ("view_appointment", "Can see appointments"),
            )

    def clean(self):
        if Appointment.objects.filter(worker=self.worker, project=self.project,
                                      during__overlap=self.during).exclude(pk=self.pk):
            raise ValidationError(_('Appointments may not overlap for same (worker, project) pair'))

    def __str__(self):
        return "Appointment {}".format(self.uuid)


class Actual(TimeStampedModel):
    uuid = ShortUUIDField()
    hours = models.DecimalField(max_digits=5, decimal_places=1)
    task_id = models.CharField(max_length=20, null=True)
    during = DateRangeField()
    billing_dollars = models.DecimalField(max_digits=10, decimal_places=2, null=True)
    lcat = models.ForeignKey('LaborCategory', on_delete=models.CASCADE)

    project = models.ForeignKey('Project', related_name='actuals', on_delete=models.CASCADE)
    worker = models.ForeignKey('Worker', related_name='actuals', on_delete=models.CASCADE)

    def clean(self):
        if Actual.objects.filter(worker=self.worker, project=self.project, task_id=self.task_id,
                                 lcat=self.lcat,
                                 during__overlap=self.during).exclude(pk=self.pk):
            raise ValidationError(_('Actuals may not overlap for same (worker, project) pair'))

    class Meta:
        db_table = 'utilization_actual'

    @property
    def rate(self):
        if not self.billing_dollars:
            return

        return round(self.billing_dollars / self.hours, ndigits=2)

    def __str__(self):
        return "Actual {}".format(self.uuid)
