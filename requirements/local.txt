# Local development dependencies go here
-r base.txt
-r test.txt

Sphinx==1.6.5
Werkzeug==0.12.2
django-debug-toolbar==1.9.1

# improved REPL
ipdb==0.10.3
pylint
openpyxl==2.4.9
autopep8

# notebooks
jupyter
matplotlib
