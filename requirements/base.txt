# Wheel 0.25+ needed to install certain packages on CPython 3.5+
# like Pillow and psycopg2
# See http://bitly.com/wheel-building-fails-CPython-35
# Verified bug on Python 3.5.1
wheel==0.30.0

django==1.11.12

# Configuration
django-environ==0.4.4

# Forms
django-braces==1.12.0
django-crispy-forms==1.7.2

# Models
django-model-utils==3.1.1
django-autoslug==1.9.3
django-shortuuidfield==0.1.3
django-extra-fields==1.0.0

# Images
Pillow==5.1.0

# Password storage
argon2-cffi==18.1.0

# For user registration, either via email or social
# Well-built with regular release cycles!
django-allauth==0.35.0


# Python-PostgreSQL Database Adapter
psycopg2==2.7.4

# Unicode slugification
awesome-slugify==1.6.5

# Time zones support
pytz==2018.4

# Redis support
django-redis==4.9.0
redis==2.10.6

# Your custom requirements go here
django-extensions==2.0.6
factory-boy==2.9.2
py==1.5.3
django-health-check==3.5.1
djangorestframework==3.8.2
django-filter==1.1.0
django-guardian==1.4.9
django-webpack-loader==0.6.0
numpy==1.14.2
ortools==6.7.4973
